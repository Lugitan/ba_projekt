﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NikeDrowning : MonoBehaviour {
    public float DrownSpeed;
    public int tsec;
    public float sideSpeed;
    private Vector3 myVector;
    private IEnumerator coroutine;

    // Use this for initialization
    void Start () {
		
	}
    //rotate Nike to let it seems that she is drowning in water
    IEnumerator shakeNike()
    {     
        myVector = new Vector3(0, 0, sideSpeed/2);
        transform.Rotate(myVector * Time.deltaTime);
        for (int i = 0; i <= 30; i++)
        {
            yield return new WaitForSeconds(tsec);
            myVector = new Vector3(0, 0, -sideSpeed);
            transform.Rotate(myVector * Time.deltaTime);
            yield return new WaitForSeconds(tsec);
            myVector = new Vector3(0, 0, sideSpeed*2);
            transform.Rotate(myVector * Time.deltaTime);
        }
    }
    // Update is called once per frame
    void Update () {
        coroutine = shakeNike();
        StartCoroutine(coroutine);
        //move Nike downwards
        transform.Translate(Vector3.down * DrownSpeed * Time.deltaTime, Space.World);
    }

}
