﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Timer : MonoBehaviour {
    //text where timer is displayed
    public Text time;
    //timer is set to 0
    private static float timer = 0;
    //start time is set to 3 hours and 3 sec
    private static float startTime = 10803;
    //for don't destroy on load
    public static Timer instance = null;
    //source: https://answers.unity.com/questions/747603/how-does-dontdestroyonload-work-1.html

    
    // Use this for initialization
    void Start () {
        //start timer
        if(timer == 0)
            timer = startTime;
        Awake();
	}
	
	// Update is called once per frame
	void Update () {
        // calculate rest time in timer
        timer -= Time.deltaTime;
        int hours = (int)timer / 3600;
        int minutes = ((int)timer % 3600) / 60;
        int seconds = (int)timer % 60;
        //print left time and format string to 00:00:00
        time.text = hours.ToString("00") + ":" + minutes.ToString("00") + ":" + seconds.ToString("00");
        if (timer < 0)
        {
            //GameOver
            SceneManager.LoadScene("GameOver");
        }
        //destroy timer if game is won
        if (GameMechanic.getWonGame())
        {
            Destroy(gameObject);
            timer = startTime;
        }
    }

    public static float getTime()
    {
        return timer;
    }

    public static float getStartTime()
    {
        return startTime;
    }
    
    private void Awake()
    {
        //Check if instance already exists
        if (instance == null)
        {
            //if not, set instance to this
            instance = this;
        }
        //If instance already exists and it's not this:
        else if (instance != this)
        {
            Destroy(gameObject);
        }

        //Sets this to not be destroyed when reloading scene
        DontDestroyOnLoad(gameObject);
    }
}
