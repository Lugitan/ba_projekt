﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class MoveAway : MonoBehaviour
{
    //arrow down to move away
    public Button down;
    // Use this for initialization
    void Start()
    {
        //press button
        down.onClick.AddListener(TaskOnClick);
    }
    //load Scene which seems that user has moved away
    void TaskOnClick()
    {
        SceneManager.LoadScene("WellSystem");
    }
}


