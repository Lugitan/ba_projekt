﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TurnCamera : MonoBehaviour {
    public GameObject Camera;
    //left button
    public Button leftButton;
    public GameObject left;
    //right button
    public Button rightButton;
    public GameObject right;
    //Vector showing change of direction
    Vector3 Vright;
    Vector3 Vleft;

	// Use this for initialization
	void Start () {
        //set vector to change direction to left or right
        Vright = new Vector3(0,5,0);
        Vleft = new Vector3(0,-5,0);
        

    }
    void TurnLeft()
    {
        Camera.transform.Rotate(Vleft, Time.deltaTime);
    }
    void TurnRight()
    {
        Camera.transform.Rotate(Vright, Time.deltaTime);
    }
    // Update is called once per frame
    void Update () {
        //press Buttons
            leftButton.onClick.AddListener(TurnLeft);
            rightButton.onClick.AddListener(TurnRight);
	}
}
