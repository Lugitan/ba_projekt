﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CamSwitch : MonoBehaviour
{
    //switch AR camera to camera
    public GameObject mainCamera;
    public GameObject ARCamera;
    // Use this for initialization
    void Start()
    {
        mainCamera.SetActive(true);
        ARCamera.SetActive(false);
    }
}