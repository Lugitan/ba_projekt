﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using Vuforia;
using UnityEngine.UI;

public class GameMechanic : MonoBehaviour
{
    public InfoButtonHandler infoHandler;
    public ChangeScene changeScene;

    public Text hintText;
    public HintButtonHandler m_hintHandler;
    public GameObject answerText;
    public GameObject dialogBox;
    public Text titleText;


    //inputfield for solution
    public InputField inputField;
    public GameObject inputParent;
   

    public GameObject goToNextSceneButton;
    public GameObject fakeFoundButton;

    //boat, which should be shown when faking capturing
    public GameObject prora;

    //bool to find the current scene
    public bool isDiscobolusScene = false;
    public bool isZeusScene = false;
    public bool isAthenaScene = false;

    //Modeltargetbehaviour to show and hide 2D Outline
    public ModelTargetBehaviour modelTarget;

    private DefaultTrackableEventHandler dbTrack;
    public string nextScene = "";

    //solution bool to check if solution 1,2 and 3 are correct
    private bool solution = false;

    //initial time of timer
    private static float timeInit;
    //time left at the timer
    private static float timeLeft;

    //check if statue was tracked and the game won
    private static bool foundStatue = false;
    private static bool pressedFoundButton = false;
    private static bool wonGame = false;

    // Descriptions for the dialogbox that is shown for found Statues
    private static string discobulusFound = "Diskobol: \"Wenn man Nike hört, denken viele zuerst an die Sportmarke Nike. Das liegt an der nahen Verbindung, den die Sportmarke mit der Nike, also der griechischen Siegesgöttin, haben möchte. So liessen sie sich auch bei ihrem Logo von der Flügelstellung der Nike von Samothrake inspirieren, um ihre Marke mit Sieg und Erfolg zu verknüpfen. Auch im alten Griechenland hofften Sportler wie ich auf Nikes Gunst, um einen Siegeskranz und den damit verbunden Ruhm zu gewinnen.\"";
    private static string athenaFound = "Athene: \"Die Nike Statue wird oft mit mir zusammen gezeigt, da wir gut befreundet sind. Sie wird allerdings auch oft in Statuen anderer Götter wie Zeus gezeigt, um zu zeigen, dass sie uns bei unserem Sieg unterstützt hat.\"";
    private static string zeusFound = "Zeus: \"Nike und ich sind gut befreundet, seit sie mir bei dem Kampf gegen die Titanen geholfen hat. Seitdem wohnt sie mit ihren Geschwistern auch im Olymp.\"";

    public enum Statues
    {
        Discobolus = 0,
        Zeus = 1,
        Athena = 2
    }

    // Use this for initialization
    void Start()
    {
        //check if statue was found
        dbTrack = modelTarget.GetComponent<DefaultTrackableEventHandler>();
        foundStatue = dbTrack.found;
        
        //no text in the beginning
        titleText.text = "";

        //player has to win the game to change it to true
        wonGame = false;

        //set inital time of timer
        timeInit = Timer.getStartTime() - Timer.getTime();

        //get time from timer script
        timeLeft = Timer.getTime();

        //hide 2D outline of Modeltargets
        modelTarget.GuideViewMode = ModelTargetBehaviour.GuideViewDisplayMode.NoGuideView;
        goToNextSceneButton.SetActive(false);

        if(nextScene == "")
        {
            Debug.Log("You haven't specified the next Scene!");
        }
        changeScene.SetScene(nextScene);
        goToNextSceneButton.GetComponentInChildren<Button>().onClick.AddListener(changeScene.LoadNewScene);

        //deactivate and hide boat
        prora.SetActive(false);
        //button deactivated until capturing
        fakeFoundButton.SetActive(false);
        //invisible marker button calling method to fake capturing
        fakeFoundButton.GetComponentInChildren<Button>().onClick.AddListener(FoundButtonPressed);

        ShowHint(0);
        dialogBox.SetActive(false);
        answerText.SetActive(false);
        showFirstDialog = true;
    }

    //method to fake that Statue has been captured
    void FoundButtonPressed()
    {
        pressedFoundButton = true;
        foundStatue = true;
        prora.SetActive(true);
    }

    private bool showFirstDialog;
    // Update is called once per frame
    void Update()
    {
        if (pressedFoundButton == false)
        {
            foundStatue = dbTrack.found;
        }

        //hints for Discobolus statue
        if (!solution)
        {
            //discobolusHints();
            if ((int)timeLeft <= (timeInit - 1200))
            {
                ShowAnswer();
            }

        }
        //if statue was tracked
        if (foundStatue == true || dbTrack.found)
        {
            //hide outline of statue and visualize information
            goToNextSceneButton.SetActive(true);
            modelTarget.GuideViewMode = ModelTargetBehaviour.GuideViewDisplayMode.NoGuideView;
            titleText.text = "Du hast das erste Bootsteil gefunden.";

            SetFoundInfo();
            inputParent.SetActive(false);


            if (showFirstDialog)
            {
                if(m_hintHandler != null)
                {
                    m_hintHandler.m_hintButton.isOn = true;
                    m_hintHandler.UpdateHintButton();
                }
                infoHandler.ShowInfo();
                showFirstDialog = false;
            }
            //if AThena scene is solved the game is won
            if(isAthenaScene)
            {
                //Debug.Log(GameMechanic.getWonGame());
                wonGame = true;
            }
        }
    }


    //clears Text of Input
    public void inputClear()
    {
        inputField.Select();
        inputField.text = "";
    }

    //check if the answer typed in is correct
    public void checkText()
    {
        if ((((inputField.text.ToLower() == "diskobol") || (inputField.text.ToLower() == "diskobol des myron")) && isDiscobolusScene) ||
            (((inputField.text.ToLower() == "athene") || (inputField.text.ToLower() == "athena")) && isAthenaScene) ||
            (((inputField.text.ToLower() == "zeus")) && isZeusScene))
        {
            fakeFoundButton.SetActive(true);
            inputClear();
            inputParent.SetActive(false);
            titleText.text = "Korrekt. Scanne jetzt die Statue.";
            modelTarget.GuideViewMode = ModelTargetBehaviour.GuideViewDisplayMode.GuideView2D;
            solution = true;
        }
        else
        {
            inputClear();
            titleText.text = "Leider Falsch.";
        }
    }

    //set timer to the left time
    public static void deactivate()
    {
        timeInit = (int)timeLeft;
    }

    //show the hints according to their scene
    public void ShowHint(int i)
    {
        if (isDiscobolusScene)
        {
            discobolusHints(i);
        }
        else if (isAthenaScene)
        {
            athenaHints(i);
        }
        else if (isZeusScene)
        {
            zeusHints(i);
        }
        else
        {
            Debug.Log("You haven't specified which is the current Scene!");
        }
    }

    public void discobolusHints(int i)
    {
        //Discobolus hint1
        if (i == 0)
        {
            hintText.text = "Wie heißt die Statue des Sportlers, welcher eine olympische Disziplin der Antike ausführt.";
        }
        //Discobolus hint2
        if (i == 1)
        {
            hintText.text = "Seine Disziplin war Teildisziplin des Fünfkampfes (Pentahlon).";
        }
        //Discobolus hint3
        if (i == 2)
        {
            hintText.text = "Die Disziplin war nicht Sprung, Lauf, Speerwurf oder Ringkampf.";
        }
        //Discobolus hint4
        if (i == 3)
        {
            hintText.text = "Der Sportler wirft eine Diskusscheibe";
        }
    }
    public void zeusHints(int i)
    {
        //Zeus hint1 
        if (i == 0)
        {
            hintText.text = "Suche nun den griechischen Göttervater.";
        }
        //Zeus hint2
        if (i == 1)
        {
            hintText.text = "Er ist auch als Gott des Blitzes und Donners bekannt.";
        }
        //Zeus hint3
        if (i == 2)
        {
            hintText.text = "Seine Frau ist Hera und er ist der Vater vieler bekannter Götter und Halbgötter.";
        }
        //Zeus hint4
        if (i == 3)
        {
            hintText.text = "Er ist der Sohn von Kronos und Rhea und seine Geschwister sind Demeter, Hades, Hera, Hestia und Poseidon.";
        }
    }

    public void athenaHints(int i)
    {

        //Athena hint1
        if (i == 0)
            hintText.text = "Suche als letzte jetzt die Göttin der Weisheit.";
        //Athena hint2
        if (i == 1)
        {
            hintText.text = "Sie ist auch die Göttin der Strategie, des Kampfes, der Kunst und des Handwerks.";
        }
        //Athena hint3
        if (i == 2)
        {
            hintText.text = "Sie ist die Tochter von Zeus und Metis.";
        }
        //Athena hint4
        if (i == 3)
        {
            hintText.text = "Sie ist die Schutzgöttin der Stadt Athen.";
        }
    }

    //if user didn't find the answer in a certain time he will get the answer
    public void ShowAnswer()
    {
        string answer = "";
        if(isAthenaScene)
        {
            answer = "Die gesuchte Göttin heißt Athene.";
        }
        else if(isDiscobolusScene)
        {
            answer = "Der Sportler heißt Diskobol.";
        }
        else if (isZeusScene)
        {
            answer = "Der gesuchte Gott heißt Zeus.";
        }

        answerText.GetComponent<Text>().text = answer;
        answerText.SetActive(true);
    }

    //infotext should be shown when game is won
    public void SetFoundInfo()
    {
        string infoText = "";
        if (isAthenaScene)
        {
            infoText = athenaFound;
        }
        else if (isDiscobolusScene)
        {
            infoText = discobulusFound;
        }
        else if (isZeusScene)
        {
            infoText = zeusFound;
        }

        dialogBox.transform.Find("Text").GetComponentInChildren<Text>().text = infoText;
    }

    //check if game was won
    public static bool getWonGame()
    {
        return wonGame;
    }
    public static void setWonGame(bool b)
    {
        wonGame = b;
    }

    //get solutions of each scene
    public static string[] getSolutions()
    {
        string[] solutions = new string[4];
        solutions[0] = discobulusFound;
        solutions[1] = zeusFound;
        solutions[2] = athenaFound;

        return solutions;
    }

}
