﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Billboard : MonoBehaviour {

    public Camera m_Camera;

    public bool isCameraUp = false;

    void Update()
    {
        if (isCameraUp)
        {
            transform.LookAt(transform.position + m_Camera.transform.rotation * Vector3.forward, m_Camera.transform.rotation * Vector3.up);
        }
        else
        {
            transform.LookAt(transform.position + m_Camera.transform.rotation * Vector3.forward, Vector3.up);
        }
    }
}
