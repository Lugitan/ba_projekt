﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class MoveClose : MonoBehaviour
{
    public Button up;

    // Use this for initialization
    void Start()
    {
        up.onClick.AddListener(TaskOnClick);
    }
    //change scene to make it look as if the visitor stands closer to Nike's well-system
    void TaskOnClick()
    {
        SceneManager.LoadScene("WellSystemClose");
    }
}

