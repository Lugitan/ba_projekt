﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HintButtonHandler : MonoBehaviour {

    public Toggle m_hintButton;
    public GameObject m_hintPanel;

    private string m_closeSring = "Schließen";
    private string m_hintString = "Hinweis";

	// Use this for initialization
	void Start () {
        UpdateHintButton();
    }
	
	// Update is called once per frame
	void Update () {
		
	}

    public void UpdateHintButton()
    {
        if(m_hintPanel != null)
        {
            m_hintPanel.SetActive(!m_hintButton.isOn);
        }

        if(m_hintButton != null)
        {
            if (m_hintButton.isOn)
            {
                m_hintButton.transform.Find("Label").GetComponentInChildren<Text>().text = m_hintString;
            }
            else
            {
                m_hintButton.transform.Find("Label").GetComponentInChildren<Text>().text = m_closeSring;
            }
        }
    }
}
