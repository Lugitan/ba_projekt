﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class UIController : MonoBehaviour {

    // Controller class to set all properties of the different handlers

    public MenuNavigationHandler m_mainHandler;
    public SubNavigationHandler m_subHandler;
    public InfoButtonHandler m_infoHandler;
    public HelpNavigationHandler m_helpHandler;
    public ChangeScene m_changeScene;
    public Sprite m_loadingIcon;
    public Sprite m_closeIcon;

    public bool m_needsArVrButton = true;
    public bool m_needsInventoryButton = true;
    public bool m_needsInfoButton = false;
    public bool m_needsReloadButton = false;

    //Set to true if the Tutorial should be started when the scene opens
    public bool m_showTutorialFirst = false;

    public ArVrButtonInfo m_arVrButtonInfo = new ArVrButtonInfo();
    public InventoryButtonInfo m_inventoryButtonInfo = new InventoryButtonInfo();
    public InfoButtonInfo m_infoButtonInfo = new InfoButtonInfo();
    public ReloadButtonInfo m_reloadButtonInfo = new ReloadButtonInfo();

    private int m_currentPanel;
    private int m_goBackToPanel;
    private ModifiedViewTrigger m_modViewTrigger;

    public enum MainPanels
    {
        Menu = 0,
        Glossary = 1,
        MainScreen = 2,
        Profile = 3,
        Help = 4
    }

    [System.Serializable]
    public class ArVrButtonInfo
    {
        public Button m_button;
        public string m_changeToScene;
        public Sprite m_ar;
        public Sprite m_vr;
        public bool m_sceneIsAR;
        public bool m_playInSameScene;
        public GameObject m_title;
    }

    [System.Serializable]
    public class InventoryButtonInfo
    {
        public Button m_button;
        public GameObject m_slideInPanel;
        public GameObject m_finalPanelPos;
        public float m_slideDuration = 0.3f;
        public AnimationCurve m_curve = AnimationCurve.Linear(0, 0, 1, 1);
    }

    [System.Serializable]
    public class InfoButtonInfo
    {
        public Button m_button;
        public GameObject m_infoPanel;
        public string m_title;
    }

    [System.Serializable]
    public class ReloadButtonInfo
    {
        public Button m_button;
    }

    // dynamically enables or disables all buttons accoring to the values set in the Unity inspector
    public void OnValidate()
    {
        SetButtonColor(m_arVrButtonInfo.m_button, m_needsArVrButton);
        SetButtonColor(m_inventoryButtonInfo.m_button, m_needsInventoryButton);
        SetButtonColor(m_infoButtonInfo.m_button, m_needsInfoButton);
        SetButtonColor(m_reloadButtonInfo.m_button, m_needsReloadButton);

        if (m_arVrButtonInfo.m_button != null && m_needsArVrButton)
        {
            if (m_arVrButtonInfo.m_sceneIsAR && m_arVrButtonInfo.m_ar != null)
            {
                m_arVrButtonInfo.m_button.gameObject.transform.Find("Image").GetComponentInChildren<Image>().sprite = m_arVrButtonInfo.m_vr;
            }
            else if(m_arVrButtonInfo.m_vr != null)
            {
                m_arVrButtonInfo.m_button.gameObject.transform.Find("Image").GetComponentInChildren<Image>().sprite = m_arVrButtonInfo.m_ar;
            }
        }
    }

    // Apply all properties when the app is started
    public void Start()
    {

        if (m_showTutorialFirst)
        {
            m_mainHandler.TutorialStarted = true;
            ShowTutorialFirst();
        }

        // Sets the arVrButton according to defined properties
        if (m_needsArVrButton && m_arVrButtonInfo.m_button != null)
        {
            //if (m_arVrButtonInfo.m_sceneIsAR)
            //{
            //    m_mainHandler.SetTitle("Augmented Reality");
            //}
            //else
            //{
            //    m_mainHandler.SetTitle("Virtual Reality");
            //}

            if (m_arVrButtonInfo.m_changeToScene != "" && !m_arVrButtonInfo.m_playInSameScene)
            {
                SetButtonScene(m_arVrButtonInfo.m_button, m_arVrButtonInfo.m_changeToScene);
                SetButtonClickedIcon(m_arVrButtonInfo.m_button, m_loadingIcon);
            }
            else if (m_arVrButtonInfo.m_playInSameScene)
            {
                m_modViewTrigger = m_arVrButtonInfo.m_button.GetComponent<ModifiedViewTrigger>();
                if(m_modViewTrigger != null)
                {
                    m_arVrButtonInfo.m_button.onClick.AddListener(m_modViewTrigger.PlayTransition);
                    m_arVrButtonInfo.m_button.onClick.AddListener(SetNewTrigger);
                }
            }
        }

        // Sets the Panel and the buttonIcon for the InventoryButton
        if (m_needsInventoryButton && m_inventoryButtonInfo.m_button != null)
        {
            SlideInPanel sP = m_inventoryButtonInfo.m_button.GetComponent<SlideInPanel>();
            m_inventoryButtonInfo.m_button.onClick.AddListener(sP.OpenPanel);
            SetButtonClickedIcon(m_inventoryButtonInfo.m_button, m_closeIcon);
            sP.SetProperties(m_inventoryButtonInfo.m_slideInPanel, m_inventoryButtonInfo.m_finalPanelPos, m_inventoryButtonInfo.m_slideDuration, m_inventoryButtonInfo.m_curve);
        }

        // Sets the InfoPanel and the callback function of the InfoButton
        if (m_needsInfoButton && m_infoButtonInfo.m_button != null)
        {
            m_infoHandler.SetInfo(m_infoButtonInfo.m_infoPanel, m_infoButtonInfo.m_title);
            //m_infoButtonInfo.m_infoPanel.SetActive(false);
        }
    }

    // function to set custom colors of enabled and disabled buttons
    public static void SetButtonColor(Button b, bool isActive)
    {
        if (!isActive && b != null)
        {
            b.gameObject.transform.Find("Image").GetComponentInChildren<Image>().color = new Color32(118, 118, 118, 255);
            b.interactable = false;
        }
        else if (b != null)
        {
            b.gameObject.transform.Find("Image").GetComponentInChildren<Image>().color = new Color32(255, 255, 255, 255);
            b.interactable = true;
        }
    }

    //Gets the ModifiedViewTrigger Script from the AR/VR Button and changes the Trigger type.
    //This is only necessary if the change between Augmented and Virtual Reality happens within the same scene
    public void SetNewTrigger()
    {
        if (m_arVrButtonInfo.m_sceneIsAR)
        {
            m_modViewTrigger.triggerType = ModifiedViewTrigger.TriggerType.VR_TRIGGER;
        }
        else
        {
            m_modViewTrigger.triggerType = ModifiedViewTrigger.TriggerType.AR_TRIGGER;
        }
    }

    // Plays the Tutorial at the start of the scene
    public void ShowTutorialFirst()
    {
        m_mainHandler.m_startPanel = ((int)MainPanels.Help);
        m_subHandler.StartSubNavigation(m_helpHandler.m_tutorialButton);
        m_helpHandler.StartHelp();
    }

    // function to call a scene change
    public void SetButtonScene(Button b, string scene)
    {
        m_changeScene.SetScene(scene); 
        b.onClick.AddListener(m_changeScene.LoadNewScene);
    }

    public void ClearButtonListeners(Button b)
    {
        b.onClick.RemoveAllListeners();
    }

    // Sets the icon of button if it has been pressed
    public void SetButtonClickedIcon(Button b, Sprite icon)
    {
        ChangeButton cB = b.GetComponent<ChangeButton>();
        if (cB == null)
        {
            b.gameObject.AddComponent<ChangeButton>();
            cB = b.GetComponent<ChangeButton>();
        }

        cB.m_newImage = icon;
    }

    public void ChangeScene()
    {
        if (m_arVrButtonInfo.m_changeToScene != "")
        {
            SceneManager.LoadScene(m_arVrButtonInfo.m_changeToScene);
        }
    }

    public bool getShowTutorial()
    {
        return m_showTutorialFirst;
    }
}
