﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ChangeScene : MonoBehaviour
{

    private string m_changeToScene;
    private GameObject m_loadingScreen;

    // sets loadingscreen panel to not show when starting the app
    void Start()
    {
        m_loadingScreen = transform.Find("LoadingScreen").gameObject;
        ShowLoadingScreen(false);
    }

    // LoadNewScene can be called by onClick event of a button
    public void LoadNewScene()
    {
        ShowLoadingScreen(true);

        if (m_changeToScene != "")
            SceneManager.LoadScene(m_changeToScene);
    }

    // LoadNewScene can be called by an external class with custom scene string
    public void LoadNewScene(string scene)
    {
        ShowLoadingScreen(true);

        if (m_changeToScene != "")
            SceneManager.LoadScene(scene);
    }

    // LoadNewScene can be called with custom LoadingScreenPanel and scene string
    public void LoadNewScene(GameObject loadingScreen, string newScene)
    {
        ShowLoadingScreen(true);

        if (newScene != "")
            SceneManager.LoadScene(newScene);
    }

    public void ShowLoadingScreen(bool b)
    {
        if (m_loadingScreen != null)
            m_loadingScreen.SetActive(b);
    }

    public void SetScene(string scene)
    {
        m_changeToScene = scene;
    }

    public void SetLoadingScreen(GameObject screen)
    {
        m_loadingScreen = screen;
    }
}
