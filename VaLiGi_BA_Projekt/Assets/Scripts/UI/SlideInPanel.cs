﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SlideInPanel : MonoBehaviour
{
    // This class is used to slide in the inventory panel

    private GameObject m_slideInPanel;
    private GameObject m_finalPanelPos;
    private float m_slideDuration = 0.3f;
    private AnimationCurve m_curve = AnimationCurve.Linear(0, 0, 1, 1);

    private static bool m_alreadyChanging = false;
    private bool m_isOpen = false;
    private Vector3 m_oldPos;
    private Vector3 m_newPos;

    // Sets all properties according to the ones set in UIController
    public void SetProperties(GameObject slideInPanel, GameObject finalPosPanel, float slideDuration, AnimationCurve curve)
    {
        m_slideInPanel = slideInPanel;
        m_finalPanelPos = finalPosPanel;
        m_slideDuration = slideDuration;
        m_curve = curve;

        m_oldPos = m_slideInPanel.transform.position;
        m_newPos = m_finalPanelPos.transform.position;
    }

    // callback function for the InventoryButton onClick event
    public void OpenPanel()
    {
        // save old position for next activation
        m_oldPos.y = m_slideInPanel.transform.position.y;
        m_newPos.y = m_finalPanelPos.transform.position.y;

        float startTime = Time.time;

        // if the panel is still moving then the button cannot be pressed again, otherwise start the movement
        // also distinguished between the panel being open or not
        if (!m_alreadyChanging && !m_isOpen)
        {
            gameObject.GetComponent<Button>().interactable = false;
            m_alreadyChanging = true;
            m_isOpen = true;
            StartCoroutine(SlidePanel(m_slideDuration, startTime, m_oldPos, m_newPos));
        }
        else if (!m_alreadyChanging)
        {
            gameObject.GetComponent<Button>().interactable = false;
            m_alreadyChanging = true;
            m_isOpen = false;
            StartCoroutine(SlidePanel(m_slideDuration, startTime, m_newPos, m_oldPos));
        }
    }

    // Courutine to move the panel smoothly according to a smoothing ratio set in an animationcurve
    private IEnumerator SlidePanel(float duration, float startTime, Vector3 oldPos, Vector3 newPos)
    {
        float deltaTime = Time.time - startTime;
        while (deltaTime <= duration)
        {
            float ratio = m_curve.Evaluate(deltaTime / duration);
            m_slideInPanel.transform.position = Vector3.Lerp(oldPos, newPos, ratio);
            yield return null;
            deltaTime = Time.time - startTime;
        }
        m_slideInPanel.transform.position = newPos;

        m_alreadyChanging = false;
        gameObject.GetComponent<Button>().interactable = true;
    }
}
