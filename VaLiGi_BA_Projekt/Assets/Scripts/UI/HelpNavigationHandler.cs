﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HelpNavigationHandler : MonoBehaviour
{

    public GameObject m_helpOverviewPanel;
    public GameObject m_cancelHelpButton;
    public GameObject m_helpButton;
    public GameObject m_topLeftUIGroup;
    public GameObject m_TopRightUIGroup;
    public SubNavigationHandler m_subNavigationHandler;
    public Button m_tutorialButton;

    // Set standard Help Button if no tutorial is currently playing
    private void Update()
    {
        if (!m_cancelHelpButton.activeSelf && m_helpOverviewPanel.activeSelf)
        {
            m_helpButton.SetActive(true);
        }
    }

    // Disable standard helpMenuButton, enable cancelHelpButton alongside with starting the Tutorial 
    public void StartHelp()
    {
        m_cancelHelpButton.SetActive(true);
        m_helpButton.SetActive(false);
        m_helpOverviewPanel.SetActive(false);
        m_subNavigationHandler.SetHelpStarted(true);
        SetUIGroups(true);
    }

    // Setting all elements back to standard display
    public void CancelHelp()
    {
        m_cancelHelpButton.SetActive(false);
        m_subNavigationHandler.ClearNavigation();
        m_helpButton.SetActive(true);
        m_helpOverviewPanel.SetActive(true);
        m_subNavigationHandler.SetHelpStarted(false);
        SetUIGroups(false);
    }

    // Called when navigation in the MainMenu. Helpoverview will be disabled and TutorialNavigation is being cancelled
    public void CancelHelpNavigation()
    {
        m_cancelHelpButton.SetActive(false);
        m_subNavigationHandler.ClearNavigation();
        m_helpButton.SetActive(true);
        m_helpOverviewPanel.SetActive(false);
        m_subNavigationHandler.SetHelpStarted(false);
        SetUIGroups(false);
    }

    public void SetUIGroups(bool b)
    {
        m_topLeftUIGroup.SetActive(b);
        m_TopRightUIGroup.SetActive(b);
    }
}
