﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SubButtonInfo : MonoBehaviour {

    public GameObject m_thisPanel;
    public GameObject m_subPanel;

    public GameObject GetThisPanel()
    {
        return m_thisPanel;
    }

	public GameObject GetSubPanel()
    {
        return m_subPanel;
    }
}
