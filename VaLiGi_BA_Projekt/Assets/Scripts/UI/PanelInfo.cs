﻿using System.Collections;
using System.Collections.Generic;

using UnityEngine;
using UnityEngine.UI;
public class PanelInfo : MonoBehaviour
{

    // This class is used to set default Panel information of mainMenuPanels
    // Makes reassigning default values of the MenuNavigationHandler m_panels list easier

    public string m_setTitle;
    public GameObject m_callButton;
    public bool m_isMainScreen;

    public string GetTitle()
    {
        return m_setTitle;
    }

    public GameObject GetCallButton()
    {
        return m_callButton;
    }

    public bool GetIsMainScreen()
    {
        return m_isMainScreen;
    }
}