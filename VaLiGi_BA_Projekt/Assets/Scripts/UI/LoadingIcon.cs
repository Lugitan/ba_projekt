﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

// Source: https://www.salusgames.com/2017/01/08/circle-loading-animation-in-unity3d/

public class LoadingIcon : MonoBehaviour
{
    // Test Class for a rotating Loadingicon on a sceneswitch
    public float m_rotateSpeed = 0.2f;
    private RectTransform m_rectComponent;
    private Image m_loadingIcon;

    private void Start()
    {
        m_rectComponent = GetComponent<RectTransform>();
        m_loadingIcon = m_rectComponent.GetComponentInChildren<Image>();
    }

    private void Update()
    {
        m_rectComponent.Rotate(0f, 0f, m_rotateSpeed * Time.deltaTime);
    }
}
