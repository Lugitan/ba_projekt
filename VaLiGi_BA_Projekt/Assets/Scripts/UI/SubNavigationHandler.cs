﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using UnityEngine.UI;

public class SubNavigationHandler : MonoBehaviour {

    public Button m_backButton;
    public Button m_InfoButton;
    public Button m_cancelHelpButton;
    public List<Panel> m_parentPanels = new List<Panel>();
    //public GameObject m_title;
    public HelpNavigationHandler m_helpNavigationHandler;

    private GameObject m_subPanel;
    private Panel m_currentPanel = new Panel();
    private string m_originalTitle;
    private string m_changedTitle;
    private bool m_HelpStarted = false;
    private bool m_isInSubNav = false;

    [System.Serializable]
    public class Panel
    {
        [SerializeField]
        public GameObject m_panel;
    }

    // function to set the indicator that a tutorial has been started
    public void SetHelpStarted(bool b)
    {
        m_HelpStarted = b;
    }

    // assign the onClick Event to the backButton when the app starts
    public void Awake()
    {
        if(m_backButton != null)
        {
            m_backButton.onClick.AddListener(() => { GoBack(); });
        }
        else
        {
            Debug.Log("Backbutton has not been assigned!");
        }
    }

    // start a subnavigation by saving the current panel in a list and activating the next panel
    public void ChangePanel()
    {
        UIController.SetButtonColor(m_InfoButton, false);
        m_isInSubNav = true;
        Panel p = new Panel();
        SubButtonInfo sBI = UnityEngine.EventSystems.EventSystem.current.currentSelectedGameObject.transform.gameObject.GetComponent<SubButtonInfo>();
        
        m_subPanel = m_currentPanel.m_panel = sBI.GetSubPanel();
        p.m_panel = sBI.GetThisPanel();

        m_subPanel.SetActive(true);
        m_parentPanels.Add(p);

        m_backButton.transform.gameObject.SetActive(true);

        // can be used if different titles have to be assigned
        //m_originalTitle = m_title.transform.GetComponentInChildren<Text>().text;
        //if(m_changedTitle != null)
        //{
        //    m_title.transform.GetComponentInChildren<Text>().text = m_changedTitle;
        //}
    }

    // possibility to start a subnavigation with a custom button externally
    public void StartSubNavigation(Button button)
    {
        UIController.SetButtonColor(m_InfoButton, false);
        m_isInSubNav = true;
        Panel p = new Panel();
        SubButtonInfo sBI = button.gameObject.GetComponent<SubButtonInfo>();
        m_subPanel = m_currentPanel.m_panel = sBI.GetSubPanel();
        p.m_panel = sBI.GetThisPanel();

        m_subPanel.SetActive(true);
        m_parentPanels.Add(p);

        m_backButton.transform.gameObject.SetActive(true);
    }

    // callback function for the BackButton to navigate to the previous panel by deactivation the currently active panel
    public void GoBack()
    {
        if(m_subPanel != null)
        {
            m_subPanel.SetActive(false);
            m_subPanel = m_parentPanels[m_parentPanels.Count - 1].m_panel;

            if (m_parentPanels.Count - 1 == 0)
            {
                m_isInSubNav = false;
                m_backButton.transform.gameObject.SetActive(false);
                UIController.SetButtonColor(m_InfoButton, true);

                // reassign old title if necessary
                //m_changedTitle = null;
                //m_title.transform.GetComponentInChildren<Text>().text = m_originalTitle;

                if (m_HelpStarted)
                {
                    m_cancelHelpButton.gameObject.SetActive(false);
                    m_HelpStarted = false;
                    m_helpNavigationHandler.SetUIGroups(false);
                }
            }

            m_parentPanels[m_parentPanels.Count - 1].m_panel.SetActive(true);
            m_parentPanels.Remove(m_parentPanels[m_parentPanels.Count - 1]);
        }
    }

    // reset the subnavigation by clearing the list and disableling all subpanel and the backbutton
    public void ClearNavigation()
    {
        UIController.SetButtonColor(m_InfoButton, true);
        m_isInSubNav = false;
        m_changedTitle = null;
        foreach (Panel p in m_parentPanels)
        {
            if(p.m_panel.tag == "UIPanel")
            {
                p.m_panel.SetActive(false);
            }
            m_subPanel.SetActive(false);
        }

        m_backButton.transform.gameObject.SetActive(false);
        m_parentPanels.Clear();
    }

    //The last panel that has been set to go back to will be set to active
    public void SetLastSubPanelActive()
    {
        if(m_parentPanels.Count > 0)
        {
            m_parentPanels[m_parentPanels.Count-1].m_panel.SetActive(true);
        }
    }

    //The CurrentPanl that is shown will be added to the SubNavigation
    public void AddCurrentPanelToSubNavigation()
    {
        m_parentPanels.Add(m_currentPanel);
    }

    public bool GetIsInSubNav()
    {
        return m_isInSubNav;
    }
}
