﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExhibitInfo : MonoBehaviour
{

    // Function can be used to test an tracked exhibit functionality with a custom InfoPanel
    // Has to be used with the InfoButtonHandler functionalities
    public InfoButtonHandler m_infoButtonHandler;
    public GameObject m_infoPanel;
    public string m_exhibitTitle;

    public GameObject GetInfoPanel()
    {
        return m_infoPanel;
    }

    public void ExhibitTracked()
    {
        m_infoButtonHandler.SetInfo(m_infoPanel, m_exhibitTitle);
    }

    public string GetExhibitTitle()
    {
        return m_exhibitTitle;
    }
}
