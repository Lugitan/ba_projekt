﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RotateImage : MonoBehaviour
{
    public float m_turns = 1.0f;

    private RectTransform m_rectComponent;
    private Vector3 m_currentRotation;
    private Vector3 m_initialRotation;
    private bool m_alreadyChanging = false;

    private void Start()
    {
        m_rectComponent = GetComponent<RectTransform>();
        m_currentRotation = m_initialRotation = gameObject.transform.rotation.eulerAngles;
    }

    //function gets called when Object is set active. Prevents Icon from getting stuck in the rotation
    private void OnEnable()
    {
        m_alreadyChanging = false;
        m_currentRotation = m_initialRotation;
    }

    private void Update()
    {
        m_rectComponent.rotation = Quaternion.Euler(m_currentRotation);
    }

    // callback function for the Icon of the button that should be rotated
    public void DisplayRotation()
    {
        m_rectComponent.localRotation = Quaternion.Euler(m_initialRotation);
        float startTime = Time.time;

        if (!m_alreadyChanging)
        {
            m_alreadyChanging = true;
            StartCoroutine(TurnIcon(m_turns, startTime));
        }
    }

    private IEnumerator TurnIcon(float duration, float startTime)
    {
        float deltaTime = Time.time - startTime;
        while (deltaTime <= duration)
        {
            m_currentRotation -= Vector3.forward * 360 * Time.deltaTime;
            yield return null;
            deltaTime = Time.time - startTime;
        }
        m_currentRotation = m_initialRotation;
        m_alreadyChanging = false;
    }
}
