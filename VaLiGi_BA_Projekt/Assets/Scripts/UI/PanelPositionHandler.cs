﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PanelPositionHandler : MonoBehaviour {

    // This class is used to reposition all panels in screen space, because all panels are distibutet in the Unity editor for easy customization purposes

    // Panel Groups should be positioned as screen space Panel, because all panels are repositioned according to their properties
    public GameObject m_mainPanelGroup;
    public GameObject m_subPanelGroup;
    public GameObject m_infoPanelGroup;

    [SerializeField]
    private GameObject[] m_otherPanelsGroup;

    private List<Panel> m_mainPanels = new List<Panel>();
    private List<Panel> m_subPanels = new List<Panel>();
    private List<Panel> m_infoPanels = new List<Panel>();
    private List<Panel> m_otherPanelsList = new List<Panel>();

    public class Panel
    {
        public GameObject m_panel;
    }

    // Use this for initialization
    void Start()
    {
        SetPanelPositions(m_mainPanelGroup);
        SetPanelPositions(m_subPanelGroup);
        SetPanelPositions(m_infoPanelGroup);

        for (int i = 0; i < m_otherPanelsGroup.Length; i++)
        {
            SetPanelPositions(m_otherPanelsGroup[i], m_mainPanelGroup.transform.position);
        }
    }

    // Sets Panel positions according to their parent object, Panels that are part of the navigation should have UIPanels set as tag
    private void SetPanelPositions(GameObject panelGroup)
    {
        if (panelGroup != null)
        {
            RectTransform[] rT = panelGroup.GetComponentsInChildren<RectTransform>();

            foreach (RectTransform r in rT)
            {
                if (r.tag == "UIPanel" || r.tag == "MainUIPanel")
                {
                    r.transform.position = panelGroup.transform.position;
                    r.transform.gameObject.SetActive(false);
                }
                else
                {
                    SetSubCatergoryPanels(r, panelGroup);
                }
            }
            panelGroup = null;
        }
    }

    // Set a Panel Group according to a set position vector
    private void SetPanelPositions(GameObject panelGroup, Vector3 position)
    {
        if (panelGroup != null)
        {
            RectTransform[] rT = panelGroup.GetComponentsInChildren<RectTransform>();

            foreach (RectTransform r in rT)
            {
                if (r.tag == "UIPanel" || r.tag == "MainUIPanel")
                {
                    r.transform.position = position;
                    r.transform.gameObject.SetActive(false);
                }
                else
                {
                    SetSubCatergoryPanels(r, panelGroup);
                }
            }
            panelGroup = null;
        }
    }

    // used to look for panels that are not part of another subgroup
    private void SetSubCatergoryPanels(RectTransform rParent, GameObject panelParent)
    {
        RectTransform[] rT = rParent.GetComponentsInChildren<RectTransform>();

        foreach (RectTransform r in rT)
        {
            if (r.tag == "UIPanel" || r.tag == "MainUIPanel")
            {
                r.transform.position = panelParent.transform.position;
                r.transform.gameObject.SetActive(false);
            }
        }
    }
}
