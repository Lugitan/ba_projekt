﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InfoButtonHandler : MonoBehaviour
{

    public Button m_infoButton;
    public Button m_closeButton;
    public GameObject m_title;
    public GameObject m_leftTopMenuGroup;
    public GameObject m_rightTopMenuGroup;
    public Color32 m_inactiveColor;
    public Color32 m_activeColor;
    public SubNavigationHandler m_subNavigationHandler;

    private GameObject m_infoPanel;
    private string m_exhibitTitle;
    private string m_savedTitle;

    // Assign CloseInfoButton functionality
    private void Awake()
    {
        if (m_closeButton != null)
        {
            m_closeButton.onClick.AddListener(() => { CloseInfo(); });
        }
        else
        {
            Debug.Log("Closebutton has not been assigned!");
        }
    }

    // Setting the panel and title that should be displayed when the InfoButton is pressed
    public void SetInfo(GameObject panel, string exhibitTitle)
    {
        m_infoPanel = panel;
        m_exhibitTitle = exhibitTitle;
        m_infoButton.transform.gameObject.GetComponent<SubButtonInfo>().m_subPanel = panel;
    }

    // callback function for the onClick event of the InfoButton
    public void ViewInfo()
    {
        if (m_infoPanel != null)
        {
            m_savedTitle = m_title.transform.GetComponentInChildren<Text>().text;
            m_title.transform.GetComponentInChildren<Text>().text = m_exhibitTitle;
            m_infoPanel.SetActive(true);
            m_leftTopMenuGroup.SetActive(false);
            m_rightTopMenuGroup.SetActive(false);

            m_closeButton.transform.gameObject.SetActive(true);
        }
    }

    // callback function for the CloseInfoButton
    public void CloseInfo()
    {
        if (m_infoPanel != null)
        {
            m_title.transform.GetComponentInChildren<Text>().text = m_savedTitle;
            m_infoPanel.SetActive(false);
            m_leftTopMenuGroup.SetActive(true);
            m_rightTopMenuGroup.SetActive(true);

            m_closeButton.transform.gameObject.SetActive(false);
            m_subNavigationHandler.ClearNavigation();
        }
    }

    // Function that will be called if the user navigates to a different panel in the MainMenu
    public void ClearInfoNavigation()
    {
        if (m_infoPanel != null)
        {
            m_infoPanel.SetActive(false);
        }
        m_closeButton.transform.gameObject.SetActive(false);
    }

    // Function that can be called if the Info has to be shown automatically without user interaction
    public void ShowInfo()
    {
        m_infoButton.onClick.Invoke();
    }

    //Functions that could be reused for future implementations

    //private void SetButtonImageColor()
    //{
    //    if (m_isActive)
    //        m_infoButton.transform.GetChild(1).GetComponentInChildren<Image>().color = m_activeColor;
    //    else
    //        m_infoButton.transform.GetChild(1).GetComponentInChildren<Image>().color = m_inactiveColor;
    //}

    //Function that can be used if the InfoButton was hidden but needs to be enabled for information display
    //public void SetInfoButtonActive(bool b)
    //{
    //    m_isActive = !m_isActive;
    //    SetButtonImageColor();
    //    m_infoButton.interactable = b;
    //}
}
