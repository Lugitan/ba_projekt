﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ChangeButton : MonoBehaviour
{

    //This class can be used to change the image and description of a button when clicked.

    public Sprite m_newImage = null;
    public string m_newDescription = null;

    private Sprite m_saveImage = null;
    private string m_saveDescription = null;

    public ChangeButton(Sprite newImage, string newDescription)
    {
        m_newDescription = newDescription;
        m_newImage = newImage;
    }

    public void Awake()
    {
        Button b = this.gameObject.GetComponent<Button>();
        b.onClick.AddListener(() => { ChangeImage(); });
    }

    // Function stores old image first and assigns new image 
    public void ChangeImage()
    {
        Debug.Log("Test");
        if (m_newImage != null)
        {
            m_saveImage = this.gameObject.transform.Find("Image").GetComponentInChildren<Image>().sprite;
            this.gameObject.transform.Find("Image").GetComponentInChildren<Image>().sprite = m_newImage;
            m_newImage = m_saveImage;
        }

        if (m_newDescription != null)
        {
            m_saveDescription = this.gameObject.transform.Find("Text").GetComponentInChildren<Text>().text;
            this.gameObject.transform.Find("Text").GetComponentInChildren<Text>().text = m_newDescription;
            m_newDescription = m_saveDescription;
        }
    }


}
