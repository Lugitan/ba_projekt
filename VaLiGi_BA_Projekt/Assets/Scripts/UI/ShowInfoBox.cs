﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ShowInfoBox : MonoBehaviour {

    public InfoButtonHandler m_infoHandler;
    public Text m_infoText;
    public Text m_pageNumbers;
    public InfoButtons m_infoButtons = new InfoButtons();
    public List<InfoBoxText> m_infoBoxTexts = new List<InfoBoxText>();

    [System.Serializable]
    public class InfoButtons
    {
        [SerializeField]
        public Button nextButton;
        public Button previousButton;
    }

    // This is used to store all Text Asset Files that will be displayed in the InfoBox
    [System.Serializable]
    public class InfoBoxText
    {
        public TextAsset m_textFile;
    }

    private string[] m_infoPages = new string[0];
    private int m_currentInfo = 0;
    private string m_allInfoNike;

    // Initialization of the Information that will be displayed alongside the pagecount
    // Additionally the strings taken from the Text assets are stored in m_infoPages array
    void Start () {
        SetInfoProperties();
        m_infoHandler.m_infoButton.onClick.AddListener(SetInfoProperties);

        if(m_infoBoxTexts.Count <= 1)
        {
            HasInfoMorePages(false);
        }

        m_infoPages = new string[m_infoBoxTexts.Count];

        if(m_infoPages.Length >= 1)
        {
            for(int i = 0; i < m_infoPages.Length; i++)
            {
                if(m_infoBoxTexts[i].m_textFile != null)
                    m_infoPages[i] = System.Text.Encoding.Default.GetString(m_infoBoxTexts[i].m_textFile.bytes);
            }
        }
    }

    // Enable or disable the page display and button navigation between pages
    public void HasInfoMorePages(bool b)
    {
        m_infoButtons.nextButton.gameObject.SetActive(b);
        m_infoButtons.previousButton.gameObject.SetActive(b);
        m_pageNumbers.gameObject.SetActive(b);
    }

    // Set the text that will be displayed in the Infobox according to the current page number
    public void SetInfoProperties()
    {
        if(m_infoPages.Length >= 1)
            m_infoText.text = m_infoPages[m_currentInfo];
        UpdatePageNumbers();
    }

    // callback function for the nextPage Button onClick event
    public void NextText()
    {
        int currentPage = m_currentInfo + 1;
        m_currentInfo = (currentPage) % m_infoPages.Length;
        m_infoText.text = m_infoPages[m_currentInfo];
        UpdatePageNumbers();
    }

    // callback function for the previousPage Button onClick event
    public void previousText()
    {
        int currentPage = (m_currentInfo + m_infoPages.Length) - 1;
        m_currentInfo = (currentPage) % m_infoPages.Length;
        m_infoText.text = m_infoPages[m_currentInfo];
        UpdatePageNumbers();
    }

    // Update the Page number display on top of the Infobox
    public void UpdatePageNumbers()
    {
        int currentPage = m_currentInfo + 1;
        m_pageNumbers.text = "" + (currentPage) + "/" + m_infoPages.Length + "";
    }
}
