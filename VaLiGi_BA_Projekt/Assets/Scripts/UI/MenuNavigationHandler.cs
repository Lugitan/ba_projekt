﻿using System.Collections;
using System.Collections.Generic;

using System;
using UnityEngine;
using UnityEngine.UI;
public class MenuNavigationHandler : MonoBehaviour
{

    public GameObject m_Canvas;
    public List<Panel> m_panels = new List<Panel>();
    public int m_startPanel = 0;
    public GameObject m_leftGroup;
    public GameObject m_rightGroup;
    public GameObject m_activeMenuBackground;
    public GameObject m_menuTitle;
    public SubNavigationHandler m_subNavigation;
    public InfoButtonHandler m_infoNavigation;
    public HelpNavigationHandler m_helpNavigationHandler;

    private bool m_tutorialStarted = false;
    private int m_currentPanel;
    private bool m_hasVR = false;
    private bool m_needsInventory = false;

    [System.Serializable]
    public class Panel
    {
        [SerializeField]
        public GameObject m_panel;
        public string m_title;
        public GameObject m_callButton;
        public bool m_isMainScreen;
    }

    // Panels are assinged into a list if the maincanvas is dragged into the m_Canvas property in Unity
    void OnValidate()
    {
        if (m_Canvas != null)
        {
            m_panels = new List<Panel>();
            RectTransform[] rT = m_Canvas.GetComponentsInChildren<RectTransform>();

            foreach (RectTransform r in rT)
            {
                if (r.transform.parent == m_Canvas.transform)
                {
                    // Setting all properties for all elements in the list by creating new Panel classes
                    PanelInfo pI = r.transform.gameObject.GetComponent<PanelInfo>();
                    Panel p = new Panel
                    {
                        m_panel = r.transform.gameObject,
                        m_title = pI.GetTitle(),
                        m_callButton = pI.m_callButton,
                        m_isMainScreen = pI.GetIsMainScreen()
                    };
                    m_panels.Add(p);
                }
            }
            // Canvas reset needs to be done in order to change properties manually in the unity inspector
            m_Canvas = null;
        }
    }

    // Disable all Panels and just show the set startPanel
    void Start()
    {
        SetPanel(m_startPanel);
    }

    // Set a Panel accoring to the position in the m_panels list
    // UI groups are enabled only if the mainPanel (AR/VR screen) is currently active
    public void SetPanel(int panelPos)
    {
        m_menuTitle.transform.GetComponentInChildren<Text>().text = m_panels[panelPos].m_title;
        if(m_panels[panelPos] != null)
            m_panels[panelPos].m_panel.SetActive(true);
        m_leftGroup.SetActive(m_panels[panelPos].m_isMainScreen);
        m_rightGroup.SetActive(m_panels[panelPos].m_isMainScreen);
        m_currentPanel = panelPos;
    }

    // callback function for the onClick event of the MainMenuButtons
    public void ChangePanel()
    {
        DisableAllPanels();
        SetActiveInMenu();
        CancelSubNavigation();

        foreach (Panel p in m_panels)
        {
            if (p.m_panel != null)
            {
                if (UnityEngine.EventSystems.EventSystem.current.currentSelectedGameObject.gameObject == p.m_callButton)
                {
                    p.m_panel.SetActive(true);

                    if (p.m_isMainScreen)
                    {
                        m_leftGroup.SetActive(true);
                        m_rightGroup.SetActive(true);
                    }
                    else
                    {
                        m_leftGroup.SetActive(false);
                        m_rightGroup.SetActive(false);
                    }

                    m_menuTitle.transform.GetComponentInChildren<Text>().text = p.m_title;
                }
            }
        }
    }

    // function that can be called if a MenuButton has been activated externally
    public void ChangePanel(Button b)
    {
        DisableAllPanels();
        SetActiveInMenu();
        CancelSubNavigation();

        foreach (Panel p in m_panels)
        {
            if (p.m_panel != null)
            {
                if (b == p.m_callButton)
                {
                    p.m_panel.SetActive(true);

                    if (p.m_isMainScreen)
                    {
                        m_leftGroup.SetActive(true);
                        m_rightGroup.SetActive(true);
                    }
                    else
                    {
                        m_leftGroup.SetActive(false);
                        m_rightGroup.SetActive(false);
                    }

                    m_menuTitle.transform.GetComponentInChildren<Text>().text = p.m_title;
                }
            }
        }
    }

    // Function that will clean all sub navigation properties if a MainMenuScreen has been switched
    public void CancelSubNavigation()
    {
        m_subNavigation.ClearNavigation();
        m_infoNavigation.ClearInfoNavigation();
        m_helpNavigationHandler.CancelHelpNavigation();
    }

    // Function that will move the background to indicate an active menu category
    public void SetActiveInMenu()
    {
        if (UnityEngine.EventSystems.EventSystem.current.currentSelectedGameObject != null)
        {
            Vector3 buttonPos = UnityEngine.EventSystems.EventSystem.current.currentSelectedGameObject.transform.position;
            Vector3 backgroundPos = m_activeMenuBackground.transform.position;

            m_activeMenuBackground.transform.position = new Vector3(buttonPos.x, backgroundPos.y, backgroundPos.z);
        }
    }

    // Sets the title on top of the screen to the according menu category that has been set in m_panels
    public void SetTitle(string s)
    {
        foreach (Panel p in m_panels)
        {
            if (p.m_panel != null && p.m_isMainScreen)
            {
                p.m_title = s;
            }
        }
    }

    // Sets all panels inactive
    public void DisableAllPanels()
    {
        foreach (Panel p in m_panels)
        {
            if (p.m_panel != null)
            {
                p.m_panel.SetActive(false);
            }
        }
    }

    public bool TutorialStarted
    {
        get
        {
            return m_tutorialStarted;
        }

        set
        {
            m_tutorialStarted = value;
        }
    }

    // Early implementation of the navigation. Could be used to improve the navigation in the future
    // Additionally a possible fadeIn/fadeOut effect can be simulated with coroutines

    //public void nextPanel()
    //{
    //    changePanel(m_currentPanel + 1, false);
    //}

    //public void previousPanel()
    //{
    //    changePanel(m_currentPanel - 1, true);
    //}

    //public void nextPanel(int panel)
    //{
    //    changePanel(m_currentPanel + panel, false);
    //}

    //public void previousPanel(int panel)
    //{
    //    changePanel(m_currentPanel - panel, true);
    //}

    //private void changePanel(int panelPos, bool isPrevious)
    //{
    //    if (panelPos < 0 || panelPos >= m_panels.Count)
    //    {
    //        Debug.Log("This Panel does not exist!");
    //    }
    //    else
    //    {
    //        //setPanels(panelPos);
    //    }
    //}

    //private IEnumerator FadeOut(Color32 start, Color32 end, float duration, int panelPos, float startTime, AnimationCurve curve, bool fade)
    //{
    //    float deltaTime = Time.time - startTime;
    //    while(deltaTime <= duration && fade)
    //    {
    //        float ratio = curve.Evaluate(deltaTime / duration);
    //        m_blackCurtain.GetComponent<Image>().color = Color32.Lerp(start, end, ratio);
    //        yield return null;
    //        deltaTime = Time.time - startTime;
    //    }
    //    m_blackCurtain.GetComponent<Image>().color = end;

    //    setPanels(panelPos);

    //    StartCoroutine(FadeIn(end, start, duration, panelPos, Time.time, curve, m_panels[panelPos].m_setFadeIn));

    //}

    //private IEnumerator FadeIn(Color32 start, Color32 end, float duration, int panelPos, float startTime, AnimationCurve curve, bool fade)
    //{
    //    float deltaTime = Time.time - startTime;
    //    while (deltaTime <= duration && fade)
    //    {
    //        float ratio = curve.Evaluate(deltaTime / duration);
    //        m_blackCurtain.GetComponent<Image>().color = Color32.Lerp(start, end, ratio);
    //        yield return null;
    //        deltaTime = Time.time - startTime;
    //    }
    //    m_blackCurtain.GetComponent<Image>().color = end;

    //    if (m_panels[panelPos].m_automaticNext)
    //    {
    //        StartCoroutine(AutomaticNext(panelPos, m_panels[panelPos].m_durationToNext));
    //    }
    //    m_alreadyChanging = false;
    //}

    //private IEnumerator AutomaticNext(int panelPos, float duration)
    //{
    //    yield return new WaitForSeconds(duration);
    //    changePanel(m_panels[panelPos].m_goToIndexPanel, false);
    //}
}
