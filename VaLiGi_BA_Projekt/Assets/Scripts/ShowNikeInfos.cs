﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ShowNikeInfos : MonoBehaviour {

    public InfoButtonHandler m_infoHandler;
    public Text m_infoText;
    public Text m_pageNumbers;
    public InfoButtons m_infoButtons = new InfoButtons();
    public TextAsset m_textFile;
    //public ScrollRect m_scrollField;

    [System.Serializable]
    public class InfoButtons
    {
        [SerializeField]
        public Button nextButton;
        public Button previousButton;
    }

    private bool m_showInfos = false;
    private string[] m_solutions;
    private int m_currentStatueInfo = 0;
    private string m_noInfoText;
    private string m_allInfoNike;

	// Use this for initialization
	void Start () {
        m_showInfos = GameMechanic.getWonGame();
        m_solutions = GameMechanic.getSolutions();
        m_noInfoText = m_infoText.text;
        SetInfoProperties();
        m_infoHandler.m_infoButton.onClick.AddListener(SetInfoProperties);

        m_allInfoNike = System.Text.Encoding.Default.GetString(m_textFile.bytes);
        m_solutions[3] = m_allInfoNike;
    }

    // Update is called once per frame
    void Update () {
        m_showInfos = GameMechanic.getWonGame();

        //m_infoButtons.nextButton.onClick.AddListener(NextText);
        //m_infoButtons.previousButton.onClick.AddListener(previousText);
	}

    public void SetInfoProperties()
    {
        m_showInfos = GameMechanic.getWonGame();

        m_infoButtons.nextButton.gameObject.SetActive(m_showInfos);
        m_infoButtons.previousButton.gameObject.SetActive(m_showInfos);
        m_pageNumbers.gameObject.SetActive(m_showInfos);


        if (m_showInfos)
        {
            m_infoText.text = m_solutions[m_currentStatueInfo];
            UpdatePageNumbers();
        }
        else
        {
            m_infoText.text = m_noInfoText;
        }
    }

    public void NextText()
    {
        int currentPage = m_currentStatueInfo + 1;
        m_currentStatueInfo = (currentPage) % m_solutions.Length;
        m_infoText.text = m_solutions[m_currentStatueInfo];
        UpdatePageNumbers();
    }

    public void previousText()
    {
        int currentPage = (m_currentStatueInfo + m_solutions.Length) - 1;
        m_currentStatueInfo = (currentPage) % m_solutions.Length;
        m_infoText.text = m_solutions[m_currentStatueInfo];
        UpdatePageNumbers();
    }

    public void UpdatePageNumbers()
    {
        int currentPage = m_currentStatueInfo + 1;
        m_pageNumbers.text = "" + (currentPage) + "/" + m_solutions.Length + "";
    }
}
