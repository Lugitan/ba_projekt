﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//
// Helps the TransitioManager to switch between AR and VR by saving the orginal transform
// and handles the different VR positions
//

public class VRLookAround : MonoBehaviour {

    [SerializeField]
    private Camera cam;
    [SerializeField]
    private Transform OriginalCAM;
    [SerializeField]
    private Transform OriginalTransform;
    [SerializeField]
    private GameObject modelToDisplay;
    [SerializeField]
    private GameObject userHead;
    [SerializeField]
    private Transform[] vrSpawns;
    [SerializeField]
    private int curNrSpawn;
    


    public void PlaceModel(GameObject newParent)
    {
        print("Redock the model.");
        modelToDisplay.transform.parent = newParent.transform;
        modelToDisplay.transform.position = newParent.transform.position;
        modelToDisplay.transform.rotation = newParent.transform.rotation;
        modelToDisplay.SetActive(true);
    }

    public void SwitchToAR()
    {
        cam.clearFlags = CameraClearFlags.SolidColor;

        userHead.transform.position = OriginalCAM.position;
        userHead.transform.rotation = OriginalCAM.rotation;

        //modelToDisplay.SetActive(false);
        OriginalTransform.gameObject.SetActive(false);
    }

    public void SwitchToVR()
    {
        cam.clearFlags = CameraClearFlags.Skybox;

        modelToDisplay.transform.parent = OriginalTransform.transform;
        modelToDisplay.transform.position = OriginalTransform.position;
        modelToDisplay.transform.rotation = OriginalTransform.rotation;

        if (vrSpawns.Length != 0)
        {
            userHead.transform.position = vrSpawns[curNrSpawn].transform.position;
            userHead.transform.rotation = vrSpawns[curNrSpawn].transform.rotation;
        }

        OriginalTransform.gameObject.SetActive(true);
        //dockingPoint.SetActive(true);
    }

    public void NextVRPosition()
    {
        curNrSpawn = mod(curNrSpawn + 1, vrSpawns.Length);

        userHead.transform.position = vrSpawns[curNrSpawn].transform.position;
        userHead.transform.rotation = vrSpawns[curNrSpawn].transform.rotation;
    }

    public void BackVRPosition()
    {
        curNrSpawn = mod(curNrSpawn - 1, vrSpawns.Length);

        userHead.transform.position = vrSpawns[curNrSpawn].transform.position;
        userHead.transform.rotation = vrSpawns[curNrSpawn].transform.rotation;
    }

    protected int mod(int x, int m)
    {
        return (x % m + m) % m;
    }
}
