﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VRSpawnGizmo : MonoBehaviour {

    public Mesh mesh;

    private void OnDrawGizmos()
    {
        Gizmos.color = new Color(1, 0, 1, 0.5F);
        Gizmos.DrawMesh(mesh, this.transform.position, this.transform.rotation, this.transform.localScale);
    }
}
