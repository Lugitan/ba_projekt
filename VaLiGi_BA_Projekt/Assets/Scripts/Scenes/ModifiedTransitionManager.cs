﻿using UnityEngine;
using System.Collections.Generic;
using System.Linq;
using Vuforia;

//
// Handles the transition between AR and VR
//
public class ModifiedTransitionManager : MonoBehaviour
{
    #region PRIVATE_MEMBER_VARIABLES
    float mTransitionCursor;
    float mCurrentTime;
    bool mPlaying;
    bool mBackward;
    BlackMaskBehaviour mBlackMask;
    MixedRealityController.Mode mCurrentMode = MixedRealityController.Mode.HANDHELD_AR;
    #endregion // PRIVATE_MEMBER_VARIABLES


    #region PUBLIC_MEMBER_VARIABLES   
    public VRLookAround vrLookAround;
    static public bool isFullScreenMode = true;
    public GameObject[] AROnlyObjects;
    public GameObject[] VROnlyObjects;

    [Range(0.1f, 5.0f)]
    public float transitionDuration = 1.5f; // seconds

    public Canvas StereoViewDivider;

    public bool InAR { get { return mTransitionCursor <= 0.66f; } }
    
    public string dataSetName;

    [Header("Only necessary for the Acropolis")]
    public GameObject edificButton;
    #endregion PUBLIC_MEMBER_VARIABLES



    #region MONOBEHAVIOUR_METHODS
    void Awake()
    {
        VuforiaARController.Instance.RegisterVuforiaInitializedCallback(SetStereoViewDivider);
    }

    void OnDestroy()
    {
        VuforiaARController.Instance.UnregisterVuforiaInitializedCallback(SetStereoViewDivider);
    }


    void Start()
    {
        mBlackMask = FindObjectOfType<BlackMaskBehaviour>();
        SetBlackMaskVisible(false, 0);

        if (MainMenu.isARnotVR)
        {
            VideoBackgroundManager.Instance.SetVideoBackgroundEnabled(true);

            print("LOAD SCENE IN AR");
            ActivateDatasets(true);
            vrLookAround.SwitchToAR();

            // At start we assume we are in AR
            mTransitionCursor = 0f;

            mCurrentMode = MixedRealityController.Mode.HANDHELD_AR;
            //MixedRealityController.Instance.SetMode(MixedRealityController.Mode.HANDHELD_AR);
        }
        else
        {   
            // TODO: Causes problems in the Parthenon scene, there it only works the first time. If something else was loaded before it doesn't work anymore.

            VideoBackgroundManager.Instance.SetVideoBackgroundEnabled(false);

            isFullScreenMode = true;

            print("LOAD SCENE IN VR");
            //ActivateDatasets(false);
            vrLookAround.SwitchToVR();

            // At start we assume we are in AR
            mTransitionCursor = 1.0f;                       

            mCurrentMode = MixedRealityController.Mode.HANDHELD_VR;
            //MixedRealityController.Instance.SetMode(MixedRealityController.Mode.HANDHELD_VR);
        }

        UpdateVisibleObjects();

        mCurrentTime = Time.realtimeSinceStartup;

        //mCurrentMode = GetMixedRealityMode();      
    }

    void Update()
    {
        float time = Time.realtimeSinceStartup;
        float deltaTime = Mathf.Clamp01(time - mCurrentTime);
        mCurrentTime = time;

        // We need to check if the video background is curently enabled
        // because Vuforia may restart the video background when the App is resumed
        // even if the app was paused in VR mode
        bool isVideoCurrentlyEnabled = IsVideoBackgroundRenderingEnabled();

        MixedRealityController.Mode mixedRealityMode = GetMixedRealityMode();

        if ((mCurrentMode != mixedRealityMode) || (InAR != isVideoCurrentlyEnabled))
        {
            // mixed reality mode to switch to
            mCurrentMode = mixedRealityMode;

            // When we transition to VR, we deactivate the Datasets 
            // before setting the mixed reality mode.
            // so to reduce CPU usage, as tracking is not needed in this phase
            // (with AutoStopCameraIfNotRequired ON by default, camera/tracker
            //  will be turned off for performance optimization).

            if (mCurrentMode == MixedRealityController.Mode.HANDHELD_VR
                || mCurrentMode == MixedRealityController.Mode.VIEWER_VR)
            {
                Debug.Log("Switching to VR: deactivating datasets");
                ActivateDatasets(false);
                vrLookAround.SwitchToVR();
            }

            // As we are moving back to AR, we re-activate the Datasets,
            // before setting the mixed reality mode.
            // this will ensure that the Tracker and Camera are restarted, 
            // in case they were previously stopped when moving to VR
            // before activating the AR mode
            if (mCurrentMode == MixedRealityController.Mode.HANDHELD_AR
                || mCurrentMode == MixedRealityController.Mode.VIEWER_AR)
            {
                Debug.Log("Switching to AR: activating datasets");
                ActivateDatasets(true);
                vrLookAround.SwitchToAR();

                if (!isFullScreenMode)
                {
                    ToggleStereoView();
                }
            }

            MixedRealityController.Instance.SetMode(mCurrentMode);
            UpdateVisibleObjects();
        }

        if (mPlaying)
        {
            float fadeFactor = 0;
            if (mTransitionCursor < 0.33f)
            {
                // fade to full black in first part of transition
                fadeFactor = Mathf.SmoothStep(0, 1, mTransitionCursor / 0.33f);
            }
            else if (mTransitionCursor < 0.66f)
            {
                // between 33% and 66% we stay in full black
                fadeFactor = 1;
            }
            else // > 0.66
            {
                // between 66% and 100% we fade out
                fadeFactor = Mathf.SmoothStep(1, 0, (mTransitionCursor - 0.66f) / 0.33f);
            }
            SetBlackMaskVisible(true, fadeFactor);

            float delta = (mBackward ? -1 : 1) * deltaTime / transitionDuration;
            mTransitionCursor += delta;

            if (mTransitionCursor <= 0 || mTransitionCursor >= 1)
            {
                // Done: stop animated transition
                mTransitionCursor = Mathf.Clamp01(mTransitionCursor);
                mPlaying = false;
                SetBlackMaskVisible(false, 0);
            }
        }
    }
    #endregion // MONOBEHAVIOUR_METHODS


    #region PUBLIC_METHODS
    public void Play(bool reverse)
    {
        // dont' restart playing during a transition
        if (!mPlaying)
        {
            mPlaying = true;
            mBackward = reverse;
            mTransitionCursor = mBackward ? 1 : 0;
        }
    }

    public void ToggleStereoView()
    {
        print("Toggle Fullscreenmode");
        isFullScreenMode = !isFullScreenMode;
    }

    #endregion // PUBLIC_METHODS


    #region PRIVATE_METHODS
    void SetStereoViewDivider()
    {
        if (!isFullScreenMode)
        {
            // Activate the alignment bar if using stereo views
            //Canvas stereoDivisionCanvas = GetComponent<Canvas>();
            if (StereoViewDivider)
                StereoViewDivider.enabled = true;
        }
    }

    void ActivateDatasets(bool enableDataset)
    {
        // ObjectTracker tracks ImageTargets contained in a DataSet and provides methods for creating and (de)activating datasets.
        ObjectTracker objectTracker = TrackerManager.Instance.GetTracker<ObjectTracker>();

        if (objectTracker != null)
        {
            IEnumerable<DataSet> datasets = objectTracker.GetDataSets();

            IEnumerable<DataSet> activeDataSets = objectTracker.GetActiveDataSets();
            List<DataSet> activeDataSetsToBeRemoved = activeDataSets.ToList();

            if (activeDataSetsToBeRemoved.Count != 0)
            {

                // 1. Loop through all the active datasets and deactivate them.
                foreach (DataSet ads in activeDataSetsToBeRemoved)
                {
                    objectTracker.DeactivateDataSet(ads);
                }

            }

            // Swapping of the datasets should NOT be done while the ObjectTracker is running.
            // 2. So, Stop the tracker first.
            objectTracker.Stop();

            if (enableDataset)
            {
                // 3. Then, look up the new dataset and if one exists, activate it.
                foreach (DataSet ds in datasets)
                {
                    // TODO: could be changed according to https://library.vuforia.com/articles/Solution/How-To-Access-and-Modify-Targets-at-Run-Time.html

                    if (ds.Path.Contains(dataSetName))
                    {
                        print("Activate " + dataSetName);
                        objectTracker.ActivateDataSet(ds);
                    }                                       
                }
            }

            // 4. Finally, restart the object tracker.
            objectTracker.Start();

        }
    }
    

    MixedRealityController.Mode GetMixedRealityMode()
    {
        if (InAR)
        {
            return isFullScreenMode ?
                MixedRealityController.Mode.HANDHELD_AR : MixedRealityController.Mode.VIEWER_AR;
        }

        return isFullScreenMode ?
            MixedRealityController.Mode.HANDHELD_VR : MixedRealityController.Mode.VIEWER_VR;
    }


    void UpdateVisibleObjects()
    {
        // VR objects
        foreach (var go in VROnlyObjects)
        {
            go.SetActive(!InAR);
        }

        // AR objects
        foreach (var go in AROnlyObjects)
        {
            go.SetActive(InAR);
        }

        if (!InAR)
        {
            if(edificButton != null) edificButton.SetActive(false);
        } 
    }

    void SetBlackMaskVisible(bool visible, float fadeFactor)
    {
        if (mBlackMask)
        {
            mBlackMask.GetComponent<Renderer>().enabled = visible;
            mBlackMask.SetFadeFactor(fadeFactor);
        }
    }

    bool IsVideoBackgroundRenderingEnabled()
    {
        var backgroundPlaneBehaviour = GetCameraRigRoot().GetComponentInChildren<BackgroundPlaneBehaviour>();
        return (backgroundPlaneBehaviour && backgroundPlaneBehaviour.GetComponent<MeshRenderer>().enabled);
    }

    Transform GetCameraRigRoot()
    {
        var eyewear = DigitalEyewearARController.Instance;
        var vuforia = VuforiaBehaviour.Instance;
        return (eyewear.CentralAnchorPoint ? eyewear.CentralAnchorPoint.transform.root : vuforia.transform);
    }
    #endregion PRIVATE_METHODS
}
