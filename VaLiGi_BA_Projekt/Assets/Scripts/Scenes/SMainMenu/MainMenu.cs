﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using Vuforia;

//
// Handles the menu in the MainMenu scene
//
public class MainMenu: MonoBehaviour
{
    [System.Serializable]
    public struct trackedStatue
    {
        public int ID;
        public string prefabPath;
        public string picturePath;
        public string name;
        public bool trackedOnce;

        public int trackSceneID;
    }

    #region PUBLIC_MEMBERS
    public enum State { AR, VR };

    // static var accessed by the ModifiedTransitionManager to determine the state in which the scene needs to be loaded
    public static bool isARnotVR = true;

    public const string MenuScene = "1-Menu";
    public const string LoadingScene = "2-Loading";
    #endregion // PUBLIC_MEMBERS


    #region PRIVATE_MEMBERS
    [SerializeField]
    private List<trackedStatue> arObjects;
    [SerializeField]
    private List<trackedStatue> vrObjects;

    [SerializeField]
    private Canvas infoCanvasC, creditsCanvasC;

    private State currentState;

    private static int sceneIDtoLoad = 1;
    #endregion // PRIVATE_MEMBERS

    
    #region PUBLIC_METHODS

    // called to unload all loaded scenes
    public static void BackToOnlyMenu()
    {
        SceneManager.LoadScene(1);
    }
    
    public void LoadSceneWithState(int id, State state)
    {
        currentState = state;
        LoadSceneTo(id);
    }

    public void LoadSceneTo(int id)
    {     

        int sceneID = 1; // id of the main menu
        if (currentState == State.AR )
        {
            sceneID = arObjects[id].trackSceneID;
            isARnotVR = true;
        }
        else if (currentState == State.VR)
        {            
            sceneID = vrObjects[id].trackSceneID;
            isARnotVR = false;
        }

        sceneIDtoLoad = sceneID;

        UpdateConfiguration(sceneID);

        // Go to scene via the Loading Scene
        SceneManager.LoadScene(LoadingScene);
    }

    public static int GetSceneToLoad()
    {
        return sceneIDtoLoad;
    }

    public static void LoadScene(string scene)
    {
        UnityEngine.SceneManagement.SceneManager.LoadScene(scene);
    }
        
  
    ///  Updates the Vufoira dataset for each scene   
    public void UpdateConfiguration(int id)
    {

        //var config = VuforiaConfiguration.Instance;
        //var dbConfig = config.DatabaseLoad;

        //// all settings which are changed for a scene, have to be reset here
        //// because any change is persistent throughout the whole application
        //dbConfig.DataSetsToLoad = dbConfig.DataSetsToActivate = new string[0];
        //config.Vuforia.MaxSimultaneousImageTargets = 4;

        //switch (id)
        //{
        //    case (3):
        //        dbConfig.DataSetsToLoad = dbConfig.DataSetsToActivate = new[] { "_Acropolis" };
        //        break;
        //    case (4):
        //        dbConfig.DataSetsToLoad = dbConfig.DataSetsToActivate = new[] { "AthenaParthenos_madrid_Cs_P_S_LT" };
        //        break;
        //    case (5):
        //        dbConfig.DataSetsToLoad = dbConfig.DataSetsToActivate = new[] { "CrouchingAphrodite" };
        //        break;
        //    case (6):
        //        dbConfig.DataSetsToLoad = dbConfig.DataSetsToActivate = new[] { "_Parthenon_OT" };
        //        break;
        //    case (9):
        //        dbConfig.DataSetsToLoad = dbConfig.DataSetsToActivate = new[] { "_Parthenon_IT" };
        //        break;
        //    default:
        //        break;
        //}
    }

    public void LoadInfoCanvas()
    {
        infoCanvasC.sortingOrder = 99;
    }

    public void LoadCreditCanvas()
    {
        creditsCanvasC.sortingOrder = 99;
    }

    public void BackFromInfo()
    {
        infoCanvasC.sortingOrder = 2;
    }

    public void BackFromCredit()
    {
        creditsCanvasC.sortingOrder = 1;
    }
    #endregion // PUBLIC_METHODS
}
