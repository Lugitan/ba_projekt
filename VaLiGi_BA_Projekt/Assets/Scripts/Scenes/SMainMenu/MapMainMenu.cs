﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MapMainMenu : MonoBehaviour {

    [SerializeField]
    private MainMenu mainMenu;
        
    [SerializeField]
    private Canvas[] aboutCanvases;   

    private int nrCurrentAbout = 0;    
    
    public void LoadAboutCanvas(int nr)
    {
        nrCurrentAbout = nr;
        aboutCanvases[nr].gameObject.SetActive(true);
        aboutCanvases[nr].sortingOrder = 99;
    }      

    public void BackToMenu()
    {
        aboutCanvases[nrCurrentAbout].gameObject.SetActive(false);
        aboutCanvases[nrCurrentAbout].sortingOrder = 0;
    }

    public void LoadSceneInVR(int nr)
    {
        aboutCanvases[nrCurrentAbout].sortingOrder = 0;
        mainMenu.LoadSceneWithState(nr, MainMenu.State.VR);
    }

    public void LoadSceneInAR(int nr)
    {
        aboutCanvases[nrCurrentAbout].sortingOrder = 0;
        mainMenu.LoadSceneWithState(nr, MainMenu.State.AR);
    }
    


}
