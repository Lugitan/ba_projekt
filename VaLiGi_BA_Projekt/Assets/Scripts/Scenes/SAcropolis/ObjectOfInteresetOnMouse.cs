﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

//
// Holds the information for each object and handels the UI activation
//
public class ObjectOfInteresetOnMouse : MonoBehaviour
{

    [SerializeField]
    private string title;
    [SerializeField]
    private Sprite image;
    [SerializeField]
    private Renderer rend;

    ScenesMenu exhibitMenu;
    ObjectsOfInterestBroadcaster objBroadcaster;

    void Start()
    {
        ActivateRenderer(false);
        exhibitMenu = FindObjectOfType<ScenesMenu>();
        objBroadcaster = FindObjectOfType<ObjectsOfInterestBroadcaster>();
    }

    public void ActivateRenderer(bool show)
    {
        rend.enabled = show;
    }
    
    private void OnMouseDown()
    {
        if (!EventSystem.current.IsPointerOverGameObject())
        {
            if (!exhibitMenu.isAboutVisible)
            {
                if (!rend.enabled){
                    objBroadcaster.CloseAllObjects(); // close all
                    ActivateRenderer(true); // open this one

                    exhibitMenu.ActivateEdificButton(title, image);
                }
                else
                {
                    // if already renderd, close it on second click
                    ActivateRenderer(false);
                    exhibitMenu.CloseAboutScreen();
                }                
            }
        }
    }

    private void Update()
    {
        if (exhibitMenu.isAboutVisible)
        {
            if(rend.enabled)
                ActivateRenderer(false);
        }
    }

}