﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//
// Holds references to all ObjectsOfInterest   
//
public class ObjectsOfInterestBroadcaster : MonoBehaviour {

    private ObjectOfInteresetOnMouse[] objs;

	// Use this for initialization
	void Start () {
        objs = this.GetComponentsInChildren<ObjectOfInteresetOnMouse>();
	}
	
	public void CloseAllObjects()
    {
        foreach (var obj in objs)
        {
            obj.ActivateRenderer(false);
        }
    }
}
