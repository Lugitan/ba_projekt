﻿using System.Collections.Generic;
using UnityEngine;

/// <summary>
///     A dictionary that holds the info texts for the Acropolis scene
/// </summary>
public class AcropolisScreenInfo
{


    #region PRIVATE_MEMBERS

    readonly Dictionary<string, string> titles;
    readonly Dictionary<string, string> descriptions;

    #endregion // PRIVATE_MEMBERS


    #region PUBLIC_METHODS

    public string GetTitle(string titleKey)
    {
        return GetValuefromDictionary(titles, titleKey);
    }

    public string GetDescription(string descriptionKey)
    {
        return GetValuefromDictionary(descriptions, descriptionKey);
    }

    #endregion // PUBLIC_METHODS


    #region PRIVATE_METHODS

    string GetValuefromDictionary(Dictionary<string, string> dictionary, string key)
    {
        if (dictionary.ContainsKey(key))
        {
            string value;
            dictionary.TryGetValue(key, out value);
            return value;
        }

        return "Key not found. ";
    }

    #endregion // PRIVATE_METHODS


    #region CONSTRUCTOR

    public AcropolisScreenInfo()
    {

        // Init our Title Strings

        titles = new Dictionary<string, string>()
        {
            { "Acropolis", "Model der Athener Akropolis" },
            { "Parthenon", "Parthenon - Temple der Athene" },
            { "OldTemple", "Der alte Athena-Tempel" },
            { "Erechtheum", "Erechtheion" },
            { "AthenaPromachos", "Statue der Athena Promachos" },
            { "Propylaea", "Propyläen" },
            { "Nike", "Niketempel" },
            { "Brauroneion", "Brauroneion" },
            { "Chalkotheke", "Chalkotheke" },
            { "Pandroseion", "Pandroseion" },
            { "Arrephorion", "Arrephorion" },
            { "Altar", "Altar der Athena" },
            { "SanctuaryofZeus", "Heiligtum des Zeus Polieus" },
            { "SanctuaryofPandion", "Heiligtum des Pandion" }
        };
        

        // Init our Description Strings

        descriptions = new Dictionary<string, string>();

        // Acropolis
        descriptions.Add("Acropolis",
            "\n" +
            "Die Akropolis in Athen ist die wohl bekannteste Vertreterin der als Akropolis bezeichneten Stadtfestungen des antiken Griechenlands. Die Athener Akropolis mit ihren bemerkenswerten Gebäuden wird deshalb oft einfach „die Akropolis“ genannt. " +
            "Den ältesten Teil der Stadt Athen ließ Perikles nach der Zerstörung durch die Perser unter Leitung des berühmten Bildhauers Phidias von den Architekten Iktinos und Kallikrates sowie Mnesikles neu bebauen. " +
            "Auf einem flachen, 156 Meter hohen Felsen stehen die zwischen 467 v. Chr. und 406 v. Chr. erbauten Propyläen, das Erechtheion, der Niketempel und der Parthenon, in dem eine kolossale Statue der Göttin Athene aus Gold und Elfenbein stand. " +
            "Eine stark verkleinerte Replik der Statue befindet sich im Archäologischen Nationalmuseum in Athen. " +
            "Die Akropolis in Athen ist seit 1986 Teil des UNESCO-Weltkulturerbes. " +
            /*
            "\n\n" +
            "Von der Festung zum Tempelbezirk" +
            "Die Akropolis von Athen ist der große, der Stadtgöttin Athene geweihte Burgberg im Herzen von Athen.Siedlungsspuren weisen bis in die Jungsteinzeit zurück. " +
            "In mykenischer Zeit war sie ab dem 14.Jahrhundert v.Chr., als von Athen aus große Teile Attikas kontrolliert und verwaltet wurden, Sitz des Königs(Wanax). " +
            "Im 13.Jahrhundert v.Chr.wurde die später Pelargikon oder Pelasgische Mauer genannte zyklopische Wehrmauer errichtet, die auch einen Zugang zu einer Quelle mit einschloss, so dass die Wasserversorgung im Fall einer Belagerung aufrechterhalten werden konnte. " +
            "Später, im demokratischen Athen, wurde sie als Sitz der Götter(Tempelbezirk) ausgebaut und verlor ihre Verteidigungsfunktion. Nach dem Sieg über die Perser wurde Athen als Vormacht des Attischen Seebundes ab 448 v.Chr.unter Perikles zum Zentrum der hellenischen Welt. " +
            "Diese Macht und den damit verbundenen Reichtum wollte man auch durch Bauwerke demonstrieren, zumal die Perser bei ihrer Einnahme der Stadt 480 v.Chr.die Haupttempel der Akropolis aus archaischer Zeit zerstört hatten(Überreste sieht man im dortigen Museum). " +
            "\n\n" +
            "Neugestaltung unter Perikles" +
            "\n" +
            "So wurde die Akropolis unter Perikles durch die Baumeister Iktinos, Mnesikles und Kallikrates unter der Leitung des genialen Bildhauers Phidias völlig neu gestaltet. " +
            "Die Propyläen des Architekten Mnesikles entstanden als großartige Eingangsanlage am Kopf einer neuen Zugangsrampe. Im Nordflügel befand sich unter anderem eine Pinakothek. " +
            "Der als Parthenon bekannte Athena - Tempel, der Haupttempel der Anlage mit dem Bildnis der Pallas Athena, wurde neu errichtet. " +
            "Hierzu wurde der Schutt des weiter nördlich gelegenen alten Athena - Tempels zur Aufschüttung eines neuen gewaltigen Plateaus genutzt. " +
            "Dieses bildete das Fundament des neuen Tempels, den man bewusst aus der Achse des alten Tempels rückte, so dass man von den Propyläen aus das gesamte Bauwerk mit einem Blick erfassen kann und nicht nur die eine(Schmal -) Seite, " +
            "wie beim alten Tempel.Der Parthenon liegt am Kopf des alten Tempelbereiches, der heute das freie Zentrum der Anlage bildet.Hier wurden dann unter freiem Himmel die Opferhandlungen der panathenischen Festtage abgehalten. " +
            "Das an der Nordseite des Hügels gelegene Erechtheion ist der zweitgrößte Tempel der Anlage.Es handelt sich um ein mehreren Göttern und mythischen Helden der Stadt geweihtes komplexes Bauwerk. " +
            "Die Anlage verdankt ihren Namen dem mythischen König Erechtheus, dessen Palast dort gestanden haben soll und der dort auch verehrt wurde. " +
            "Im Osten beherbergte es einen Altar der Athena und das alte hölzerne Kultbild der Göttin, das im Zuge der Panathenäen neu eingekleidet wurde. " +
            "Nach Westen, etwas tiefer gelegen, befand sich der Altar des Poseidon, der den Wettstreit um die Gunst der Athener gegen die Athena verlor. " +
            "Man zeigte dort auch die Spuren, die Poseidons Dreizack im Fels hinterlassen haben soll.Der wohl bekannteste Flügel des Erechtheions ist die Korenhalle auf der südlichen, dem Parthenon zugewandten Seite, wo Frauengestalten(Karyatiden) die Säulen ersetzen. " +
            "In der Gruft darunter ruhen die athenischen Urahnen; es wurden dort auch mehrere Götter verehrt. Unmittelbar westlich des Bauwerks stand der heilige Ölbaum, den Athene den Athenern geschenkt haben soll, die die Stadt daraufhin nach ihr benannten. Heute sieht man dort einen „Nachfolger“. " +
            "Der Niketempel liegt im Westen des Burghügels, südwestlich der Propyläen, und ist der kleinste und zierlichste Tempel der Anlage.Er wird Kallikrates zugeschrieben und wurde später als die übrige Anlage errichtet. " +
            "Die Errichtung geschah nach der Regierung des Perikles und gegen dessen Wunsch. Geweiht ist er der Nike apteros, der „ungeflügelten“ Siegesgöttin. " +
            "Alle vier Jahre fanden die großen, in den Jahren dazwischen die sog.kleinen Panathenäischen Festspiele zu Ehren der Athena statt, es wurden ein baulicher Rahmen und eine Bühne erstellt. " +
            "Der Festzug ging quer durch die Stadt vom Dipylon, dem großen viertürmigen Stadttor im Kerameikos, über die Agora und die breite Rampe hinauf zur Akropolis, wo der Göttin ihr neues Gewand überreicht wurde. " +
            "Der Panathenäische Festzug ist auch das Bildmotiv des berühmten Parthenonfrieses(heute größtenteils in London, British Museum). " +
            "\n\n" +
            "<b>Bauwerke</b>" +
            "1. Test" +
            */
            "\n");

        // Parthenon
        descriptions.Add("Parthenon",
            "\n" +
            "Der Parthenon (griechisch παρθενών „Jungfrauengemach“) ist der Tempel für die Stadtgöttin Pallas Athena Parthenos auf der Athener Akropolis. " +
            "Er wurde zum Dank für die Rettung der Athener und Griechen durch die Göttin nach dem letzten Perserkrieg als dorischer Peripteros erbaut. " +
            "Im Laufe der Geschichte Griechenlands diente das Gebäude unter anderem auch als Schatzkammer des Attischen Seebunds. " +
            "\n" +
            "Der Parthenon ist eines der berühmtesten noch existierenden Baudenkmäler des antiken Griechenlands und eines der bekanntesten Gebäude weltweit. " +
            "Das Gebäude beherrscht als zentraler Bau seit fast 2.500 Jahren die Athener Akropolis. " +
            //"Der Parthenon ersetzte einen älteren Tempel der Athena, den sogenannten Vorparthenon, der während der persischen Eroberung Athens im Jahr 480 v.Chr.zerstört worden war. " +
            //"Im 6.Jahrhundert wurde der Tempel in eine Kirche umgewandelt, die der Jungfrau Maria geweiht war. " +
            "\n" +
            "Unter den Osmanen zur Moschee umgestaltet, beherbergte der Parthenon im Krieg gegen Venedig ein Munitionslager. 1687 wurde dieses von einer Kugel getroffen, wodurch es explodierte und den Tempel stark beschädigte. " +
            "Umfangreiche Teile seiner Baudekoration wurden 1801 von Lord Elgin entfernt und nach London gebracht.Der Streit über die Rückgabe dieser sogenannten Elgin Marbles hält bis heute an" +
            "\n");

        // OldTemple
        descriptions.Add("OldTemple",
            "\n"+
            "Der Alte Athena-Tempel war bis zur Zerstörung durch die Perser im Jahr 480 v. Chr. der Kultbau für Athena Polias, die Stadtgöttin Athens auf der Akropolis. " +
            "An zentraler Stelle auf der Akropolis gelegen, erhob er sich auf den Resten einer mykenischen Palastanlage. " +
            "Neben den Fundamenten wurden zahlreiche Bauglieder dorischer Ordnung gefunden, die mit dem Tempel und seinen verschiedenen Bauphasen in Verbindung gebracht werden. " +
            "\n");

        // Erechtheum
        descriptions.Add("Erechtheum",
            "\n" +
            "Das Erechtheion ist ein Tempel im ionischen Baustil auf der Akropolis in Athen, der etwa zwischen 420 und 406 v. Chr. erbaut wurde. Die Konzeption geht vermutlich auf Perikles zurück, der aber zu Baubeginn bereits verstorben war. " +
            "Als Baumeister des Tempels gelten die Architekten (Phi)lokles von Acharnai und Archilochos von Agryle, unter deren Aufsicht der Tempel um 406 v. Chr. vollendet wurde. " +
            "\n\n"+
            "Das Erechtheion steht dort, wo ursprünglich der Palast des mythischen Königs Erichthonios(Erechtheus I.) gewesen sein soll. " +
            "Der Tempel fasste in einer komplexen architektonischen Gestalt mehrere alte Kulte für insgesamt 13 Gottheiten und Heroen zusammen. " +
            "So enthielt er das hölzerne, angeblich vom Himmel gefallene Kultbild der Stadtgöttin Athene, das jährlich am Fest der Panathenäen neu geschmückt wurde. " +
            "Ferner umfasste der Bau die Erdspalte, in der eine der Athene heilige Schlange gelebt haben soll, den heiligen Ölbaum der Göttin, die Salzquelle, die Poseidon bei einem Wettstreit mit Athene entstehen ließ, und das Grab des mythischen Königs Kekrops I. " +
            "\n\n" + 
            "Bekannt ist das Erechtheion vor allem durch eine Vorhalle, die anstelle von Säulen von sechs überlebensgroßen Mädchenfiguren(korai) getragen wird. " +
            "Sie wurden auch als Karyatiden bezeichnet(laut Vitruv benannt nach der Stadt Karya auf der Peloponnes); es ist jedoch nicht gesichert, wen sie darstellen. Die Karyatiden gehören stilistisch zum Reichen Stil. " +
            "\n\n" +
             /*
             "Im Laufe seiner Geschichte wurde das Gebäude zu verschiedenen Zwecken genutzt und dabei oft die ursprüngliche Form beschädigt. Im 7.Jahrhundert wurde es in eine byzantinisch-christliche Kirche umgewandelt. " +
             "Im Jahr 1463 diente es als Harem eines Offiziers der osmanischen Armee.Eine der sechs Koren wurde 1811 von Lord Elgin nach Großbritannien gebracht und befindet sich heute im British Museum. " +
             "Die verbliebenen fünf wurden Ende des 20.Jahrhunderts durch Nachbildungen ersetzt, um weitere Beschädigungen durch Witterungseinflüsse zu verhindern.Die Originale sind im Akropolis - Museum ausgestellt. " +
              */
              "\n" );

        // AthenaPromachos
        descriptions.Add("AthenaPromachos",
            "\n" +
            "Das Standbild der Athena Promachos, der „in vorderster Linie Kämpfenden“, war eine kolossale Bronzestatue aus der Hand des Bildhauers Phidias. " +
            "Sie stand zwischen dem Eingang zur Athener Akropolis, den Propyläen, und dem Erechtheion und somit links des Prozessionswegs zum Parthenon. " +
            "Athena war die Schutzgöttin Athens, mehrere ihrer Standbilder befanden sich auf der Akropolis, unter anderem zwei weitere Werke des Phidias: " +
            "die Athena Parthenos im Parthenon und die Athena Lemnia. Der Name Athena Promachos ist zum ersten Mal im 4. Jahrhundert n. Chr. belegt. " +
            "Noch Pausanias nannte sie schlicht die große Athena aus Bronze auf der Akropolis. " +
            "\n\n" +
            /*
            " <size=42><b>Geschichte:</b></size> " +
            "Die Athena Promachos ist eines der frühesten überlieferten Werke des Phidias und wurde vor wahrscheinlich 450 v. Chr. aufgestellt, finanziert aus der Beute von der Schlacht von Marathon, " +
            "möglicherweise aber auch erst aus der Beute der Schlacht vom Eurymedon 469 v. Chr. Hierfür sprechen Abrechnungsurkunden für eine monumentale Bronzestatue. " +
            "die einen Zeitraum von neun Jahren abdecken und aufgrund epigraphischer Merkmale in die Zeit vor 450 v. Chr. datiert werden können. " +
            "Teile der marmornen und 5,50 × 5,60 Meter großen Statuenbasis sind erhalten. " +
            "Folgt man einer Beschreibung byzantinischer Zeit, die sich aller Wahrscheinlichkeit nach auf die Statue bezieht, so war sie etwas über 9 Meter hoch. " +
            "Die Bronzeplastik zeigte Athena stehend, den Schild gegen ihr Bein gelehnt, einen Speer in der Rechten. " +
            "Münzbilder der Kaiserzeit, die sicher die Athena Promachos des Phidias wiedergeben, können keinen Eindruck vom Aussehen der Statue vermitteln, lediglich der vorgestreckte rechte Arm ist zu erkennen. " +
            "Die Statue war so hoch, dass man die Spitze ihres Speers und den Helmbusch von See aus bis zur Höhe von Kap Sounion sehen konnte. " +
            "\n" +
            "Über neun Jahrhunderte stand die Athena Promachos auf der Akropolis, bis sie 465 n.Chr. nach Konstantinopel geschafft wurde. " +
            "Im Jahr 1203 wurde das Bildnis von aufgebrachten Christen zerstört, da man glaubte, es würde die belagernden Kreuzfahrer anlocken. " +
            "Unter den verschiedenen römischen Kopien, die mit der Statue in Verbindung gebracht werden, gelten die Athena Elgin im Metropolitan Museum of Art und der Torso der Athena Medici aus dem Louvre, mit seinen verschiedenen Repliken, als getreueste Überlieferungen. " +
            */"\n");

        // AthenaPromachos
        descriptions.Add("Propylaea",
            "\n" +
            "Die Propyläen (altgriech. Προπύλαια Propylaia, Plural von προπύλαιον propylaion „Vorhof, Vorhalle“) bilden den monumentalen und repräsentativen Torbau zum heiligen Bezirk der Athener Akropolis. " +
            "Sie wurden zwischen 437 und 432 v. Chr. errichtet. Spätestens mit Beginn des Peloponnesischen Krieges wurden die Arbeiten an dem noch unfertigen Bau eingestellt und nicht wieder aufgenommen. " +
            "Entwerfender Architekt war der bis dahin unbekannte Mnesikles. Der Bau war Bestandteil des perikleischen Bauprogramms auf der Akropolis und wurde in Angriff genommen. " +
            "als die Arbeiten am Parthenon zu Ende gebracht waren. Die Propyläen erhoben sich am Scheitelpunkt der westlichen Zugangsrampe, über die man gewöhnlich die Akropolis bestieg. " +
            "\n");

        // Nike
        descriptions.Add("Nike",
            "\n" +
            "Der Tempel der Athena Nike, auch kurz Niketempel oder Tempel der Nike Apteros genannt, erhebt sich auf einer kleinen Bastion südwestlich der Propyläen der Athener Akropolis. " +
            "Er ersetzte einen während der persischen Besatzung der Akropolis 480 v. Chr. zerstörten Vorgängerbau. 448 v. Chr. erging der Auftrag zum Neubau des Tempels an Kallikrates. " +
            "einen der Architekten des Parthenon. Die Bauarbeiten scheinen aber erst während des Nikiasfriedens 421 v. Chr. aufgenommen worden zu sein. Etwa 410 v. Chr. war der Bau vollendet. " +
            "\n");

        // Brauroneion
        descriptions.Add("Brauroneion",
            "\n" +
            "Das Brauroneion war das Heiligtum der Artemis Brauronia auf der Athener Akropolis, das während der Herrschaft des Peisistratos geweiht wurde. " +
            "Die brauronische Artemis, Schutzgöttin der Schwangeren und Gebärenden, hatte ihr Hauptheiligtum in Brauron, einem Demos an der Ostküste Attikas. " +
            "Der heilige Bezirk auf der Akropolis hatte einen unregelmäßig trapezförmigen Grundriss und besaß keinen Tempel. " +
            "Stattdessen diente eine Säulenhalle, eine Stoa, in dieser Funktion. Die etwa 38 Meter lange und 6,80 Meter tiefe Stoa stand an der südwestlichen Umfassungsmauer der Akropolis und öffnete sich nach Norden. " +
            //"An ihren Ecken befanden sich zwei risalitartige geschlossene Seitenflügel von etwa 9,30 Meter Länge. Der gesamte westliche, heute verlorene Teil, stand auf der mykenischen Umfassungsmauer der Akropolis. " +
            //"Von ihrer Ostseite haben sich nur Abarbeitungen im Fels, die der Aufnahme von Mauern dienten, sowie einige wenige Werkstücke aus Kalkstein erhalten. " +
            //"\n\n" +
            "In einem der Flügel wurde das hölzerne Kultbild der Gottheit aufbewahrt. " +
            "\n\n" +
            "Wandten sich Frauen um Hilfe bittend an Artemis, stifteten sie Kleidungsstücke, die dem Kultbild umgelegt wurden. " +
            /*
            "346 v.Chr. wurde ein zweites Kultbild aufgestellt, das laut Pausanias ein Werk des Praxiteles war. " +
            "\n\n" +
            "Der Eingang zu dem kleinen heiligen Bezirk befand sich nahe der Nord - Ost - Ecke und wird durch sieben in den Fels gehauene Treppenstufen markiert. " +
            "Stufen und nördliche Einfriedung wurden vermutlich von Mnesikles im Zusammenhang mit dem Bau der Propyläen angelegt. " +
            "Die Datierung der Anlage ist unsicher.Allgemein wird aber eine Bauzeit um 430 v.Chr.im Zusammenhang mit dem Bau der Propyläen angenommen. " +
            */"\n");

        // Chalkotheke
        descriptions.Add("Chalkotheke",
            "\n" +
            "Allgemein bezeichnet Chalkotheke (von griechisch χάλκεος „eisern“, „ehern“) ein zur Aufbewahrung von Weihgeschenken aus Metall dienendes Gebäude in einem Tempel oder Tempelbezirk. " +
            "\n\n" +
            "Am bekanntesten ist die Chalkotheke auf der Athener Akropolis, die durch Inschriften des 4.Jahrhunderts v.Chr.belegt ist. " +
            "Auf einem Dekret wird die Auflistung aller in der Chalkotheke aufbewahrten Gegenstände und die Aufstellung der diese Liste tragenden Stele vor der Chalkotheke angeordnet. " +
            "\n\n" +
            /*
            "Östlich des Heiligtums der Artemis Brauronia entdeckte Gebäudereste glaubt man der Chalkotheke zuweisen zu können. " +
            "Es sind nur noch die Kalksteinfundamente des Gebäudes und Felsabarbeitungen zur Aufnahme der Fundamente erhalten. " +
            "Das etwa 43 Meter lange und 14 Meter breite Gebäude stand vor der südlichen Umfassungsmauer der Akropolis. " +
            "Für einen Teil des Gebäudes, eine vorgelagerte, etwa 4, 50 Meter tiefe Säulenhalle, wurden einige Stufen der Freitreppe, die sich an seiner Nordwest - Ecke befanden und zum Bauniveau des Parthenon führten, zerstört und überbaut. " +
            "Das Gebäude selbst wird daher etwa gleichzeitig mit dem Parthenon um die Mitte des 5.Jahrhunderts v.Chr.datiert, während die Säulenhalle eine Hinzufügung des frühen 4.Jahrhunderts v.Chr.ist. " +
            "In römischer Zeit scheint das Gebäude umfangreich restauriert worden zu sein.Zahlreiche Werkstücke dieser Restaurierung, die aufgrund ihrer Abmessungen nur mit der Chalkotheke zu verbinden sind, wurden verstreut auf der Akropolis gefunden. " +
            */"\n");

        // Pandroseion
        descriptions.Add("Pandroseion",
            "\n" +
            "Das Pandroseion (griechisch Πανδρόσειον) war ein Tempel auf der Akropolis von Athen. " +
            "Er war Pandrosos, der Tochter des Kekrops I., geweiht, die als erste Priesterin der Göttin Athene galt. " +
            "Er stand auf der Nordseite des Akropolis-Plateaus – dem Schauplatz der wichtigsten attischen Mythen. " + 
            "In diesem Bereich fand man auch die ältesten baulichen Überreste aus mykenischer Zeit. " +
            "Neben der Taugöttin Pandrosos (griechisch δρόσος Tau) wurde hier mit Thallo (griechisch Θαλλώ = die Blütenbringerin) eine weitere Fruchtbarkeitsgöttin verehrt. " +
            "\n");

        // Arrephorion
        descriptions.Add("Arrephorion",
            "\n" +
            "Das Arrephorion, das Wohnhaus der Arrephoren, war eines der Gebäude auf der Athener Akropolis. Pausanias legt im Rahmen seiner Beschreibungen zum nordwestlich des Erechtheions gelegenen Areals folgendes dar:" +
            "\n" +
            "„Was mich aber am meisten in Staunen versetzte, ist nicht allen bekannt, und ich will daher berichten, was geschieht. Nicht weit vom Tempel der Polias entfernt wohnen zwei Jungfrauen, die die Athener Arrhephoroi nennen. " +
            "Diese halten sich einige Zeit bei der Göttin auf, und wenn die Zeit des Festes kommt, tun sie in der Nacht folgendes. " +
            "Sie setzen sich auf den Kopf, was ihnen die Priesterin der Athena zu tragen gibt, und dabei weiß diese nicht, was sie ihnen gibt, und die es tragen " +
            "wissen es auch nicht, und nicht weit entfernt ist in der Stadt ein Bezirk der Aphrodite in den Gärten, zu dem ein natürlicher unterirdischer Gang führt. " +
            "Dorthin steigen die Jungfrauen hinab. Unten lassen sie, was sie getragen haben, und erhalten dafür anderes und bringen es verdeckt. " +
            "Und diese entlassen sie nun von da an und führen statt ihrer andere Jungfrauen auf die Burg.“ "+
            "\n\n" +
            /*
            "Auch ein bei Plutarch überlieferter Ballspielplatz der Arrephoren[2] wird im Zusammenhang mit ihrem „Wohnhaus“ stehend gesehen. " +
            "\n" +
            "Wilhelm Dörpfeld identifizierte in den 1920er Jahren als erster die Fundamente eines Gebäudes im betreffenden Bereich als zum Arrephorion gehörig. " +
            "Vor allem die unmittelbare Lage an einem in die Unterstadt führenden Felsspalt stützt in Kombination mit der Aussage des Pausanias diese Deutung. " +
            "\n" +
            "Das aus einheitlichen Kalksteinquadern gebildete und mehrere Lagen hoch erhaltene Fundament gründet auf dem Burgfels nahe der nördlichen Akropolismauer. " +
            "Lediglich die Nordwest - Ecke des Fundamentes wird durch eine mittelalterliche Treppe gestört. " +
            "Die Dimensionen des Gebäudes erreichten bei nicht ganz gegebener Regelmäßigkeit einen fast quadratischen Grundriss von 12, 20 Meter Seitenlänge.Die Fundamentstreifen waren hierbei 1, 90–2, 00 Meter breit. " +
            "Ein mit 1, 30–1, 40 Meter etwas schmalerer Fundamentstreifen teilt das Gebäude in eine etwa 4, 40 Meter tiefe Vorhalle und einen 8 Meter tiefen, quer rechteckigen Innenraum. " +
            "\n" +
            "Aufgrund der Werktechnik und der direkten baulichen Beziehung zum nahe gelegenen Erechtheion" +
            " – das Gelände zwischen beiden Bauten wurde zum Niveauausgleich mit dem Ziel eines einheitlichen Laufhorizontes um 3 Meter aufgefüllt – " +
            "wird das Fundament in das letzte Viertel des 5.Jahrhunderts v.Chr.datiert.Wegen unsachgemäßer Ausgrabungen im 19.Jahrhundert fehlen weitere Datierungsgrundlagen. " +
            "\n" + 
            "Die Rekonstruktion der aufgehenden Architektur ist umstritten.[5] Ging Dörpfeld noch von einer nach Süden gerichteten Front mit zwei Säulen zwischen Anten aus" +
            "– eine Mutmaßung, die wirksam in letzter Zeit wieder aufgegriffen wurde[6] –, rekonstruierte man später vier Säulen zwischen den Anten. " +
            "Da weitere viersäulige Antenbauten jedoch nicht bekannt sind,[8] wurde zuletzt eine Rekonstruktion mit sechssäuliger prostyler Vorhalle vorgeschlagen. " +
            "Wegen der starken Fundamentbreiten ist von einem Stufenunterbau, einer Krepis, auszugehen, auf dem das eigentliche Gebäude stand. " +
            "Über der sechssäuligen, prostylen Vorhalle kann sich nur ein Giebel mit entsprechendem Dach erhoben haben.Ältere Rekonstruktionen mit einem Walmdach wären demnach zu verwerfen. " +
            "Welcher Säulenordnung der Bau war, ist ungeklärt, eine ionische Ordnung ist zu erwägen, obgleich die meisten vorliegenden Rekonstruktionen eine dorische Ordnung unterstellen. " +
            */"\n");

        // Altar
        descriptions.Add("Altar",
            "\n" +
            "... " +
            "\n");

        // SanctuaryofZeus
        descriptions.Add("SanctuaryofZeus",
            "\n" +
            "Das Heiligtum des Zeus Polieus, des Zeus als Beschützer der Stadt, in Athen befand sich auf dem höchsten Punkt der Akropolis an deren östlichen Ende. " +
            "Die Anlage lag etwa 8 Meter nordöstlich des Parthenon. Es handelte sich um ein weiträumiges Areal, das durch Umfassungsmauern in zwei Komplexe gegliedert war." +
            /*
            "einen rechteckigen westlichen Hofbereich von etwa 26 × 17 Meter, der durch eine kleine Toranlage an der Südwest-Ecke zugänglich war, und einen unregelmäßig gestalteten östlichen Bereich. " +
            "Dieser konnte über mehrere Zugänge, unter anderem auch von dem westlichen Hof aus, betreten werden. " +
            "An seiner Südseite haben sich die Fundamentspuren eines kleinen Antentempels, in dessen Mitte sich eine Opfergrube für die Asche von Brandopfern befand, sowie Reste eines lang gestreckten Altartisches erhalten. " +
            "\n\n" + */
            "Das Heiligtum ist verbunden mit den archaischen Riten um das Bouphonia genannte Tieropfer, das Zeus am Fest der Dipolieia im Monat Skirophorion dargebracht wurde. " +
            "\n\n" +
            "Pausanias schreibt in dem Zusammenhang:" + "\n" +
            "Da[auf der Akropolis] sind Statuen des Zeus, eine von der Hand des Leochares, eine andere des Polieus...Auf den Altar des Zeus Polieus legen sie Gerste gemischt mit Weizen und lassen beides unbewacht. " +
            "Der Ochse, den sie für das Opfer bereits vorbereitet haben, geht zu dem Altar und frisst von dem Getreide. " +
            "Einen der Priester nennen sie Ochsenschlachter; er tötet den Ochsen, wirft die Axt beiseite und rennt davon. " +
            "Die übrigen aber bringen die Axt zu Gericht und tun so, als würden sie den Täter nicht kennen. " +
            "\n\n" +
            "Das Opfer fand vermutlich an dem Altartisch vor dem kleinen Antentempel statt, während die hierfür ausgewählten Ochsen sich während des Jahres wohl in dem westlichen Hof aufhielten. " +
            "Zumindest spricht eine Inschrift aus dem Jahr 485 v.Chr., die den Umgang mit dem Dung der Ochsen regelt, für eine dauerhafte Anwesenheit der Tiere auf der Akropolis. " +
            "Die Anlage wird um 500 v.Chr.datiert. " +
            "\n");

        // SanctuaryofPandion
        descriptions.Add("SanctuaryofPandion",
            "\n" +
            "Das Heiligtum des Pandion, auch Heroon des Pandion genannt, lag an der Südost-Ecke der Athener Akropolis, etwa 25 Meter östlich des Parthenon. " +
            "Den mittlerweile durch das Akropolismuseum überbauten Fundamenten und Felsabarbeitungen nach zu urteilen, handelte es sich um einen rechteckigen, mit Mauern eingefassten Bezirk. " +
            /*
            "Er lag etwa 3,80 Meter unterhalb des Parthenon-Plateaus. Die nicht überdachte, etwa 40 Meter lange und 17,50 Meter breite Anlage war Nordwest-Südost orientiert und durch eine Mauer in zwei Bereiche geteilt:" +
            "das eigentliche Heiligtum im Westen, eine Werkstatt, Ergasterion, im Osten. " +
            "Ein Tor in der Südmauer führte in den östlichen Teil, während der westliche Bereich durch ein vorgelagertes, mit Säulen geschmücktes Propylon betreten wurde. " +
            "Ein Vorgängerbau, dessen Reste östlich erhalten sind, wurde für den Bau der Akropolismauer des 5. Jahrhunderts v. Chr. niedergelegt und machte anscheinend den Neubau um 430 v. Chr. notwendig. " +
            "\n\n" +
            */
            "Das Heiligtum beherbergte eine Statue des Pandion, Phylenheroe einer attischen Phyle und mythischer König Athens. " +
            "Möglicherweise feierten die Mitglieder der Phyle Pandionis in diesem Heiligtum auch die Pandia, ein dem Zeus geweihtes Fest. " +
            "\n\n" +
            "Das Heiligtum des Pandion wurde früher südöstlich der Propyläen lokalisiert. " +
            "\n");
    }

    #endregion // CONSTRUCTOR

}

