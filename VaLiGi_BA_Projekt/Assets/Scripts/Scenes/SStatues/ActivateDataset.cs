﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Vuforia;

//
// activates the dataset if one doesn't start from the MainMenu
//
public class ActivateDataset : MonoBehaviour {

    // specify these in Unity Inspector
    public string dataSetName = "";  //  Assets/StreamingAssets/QCAR/DataSetName
    public bool isVuforiaActive = true;

    // Use this for initialization
    void Start()
    {
#if (UNITY_EDITOR)

        MixedRealityController.Instance.SetMode(MixedRealityController.Mode.HANDHELD_AR);

        if (isVuforiaActive)
        {
            // Vuforia 6.2+
            VuforiaARController.Instance.RegisterVuforiaStartedCallback(LoadDataSet);
        }
        else
        {
            VuforiaBehaviour.Instance.enabled = false;
            VuforiaARController.Instance.RegisterVuforiaStartedCallback(DeactivateAllDataSets);
        }
#endif // UNITY_EDITOR 
    }

    void LoadDataSet()
    {
        // ObjectTracker tracks ImageTargets contained in a DataSet and provides methods for creating and (de)activating datasets.
        ObjectTracker objectTracker = TrackerManager.Instance.GetTracker<ObjectTracker>();
        IEnumerable<DataSet> datasets = objectTracker.GetDataSets();

        IEnumerable<DataSet> activeDataSets = objectTracker.GetActiveDataSets();
        List<DataSet> activeDataSetsToBeRemoved = activeDataSets.ToList();

        // 1. Loop through all the active datasets and deactivate them.
        foreach (DataSet ads in activeDataSetsToBeRemoved)
        {
            objectTracker.DeactivateDataSet(ads);
        }

        // Swapping of the datasets should NOT be done while the ObjectTracker is running.
        // 2. So, Stop the tracker first.
        objectTracker.Stop();

        // 3. Then, look up the new dataset and if one exists, activate it.
        foreach (DataSet ds in datasets)
        {
            if (ds.Path.Contains(dataSetName))
            {
                objectTracker.ActivateDataSet(ds);
            }
        }

        // 4. Finally, restart the object tracker.
        objectTracker.Start();
    }

    void DeactivateAllDataSets()
    {
        // ObjectTracker tracks ImageTargets contained in a DataSet and provides methods for creating and (de)activating datasets.
        ObjectTracker objectTracker = TrackerManager.Instance.GetTracker<ObjectTracker>();

        IEnumerable<DataSet> activeDataSets = objectTracker.GetActiveDataSets();
        List<DataSet> activeDataSetsToBeRemoved = activeDataSets.ToList();

        // 1. Loop through all the active datasets and deactivate them.
        foreach (DataSet ads in activeDataSetsToBeRemoved)
        {
            objectTracker.DeactivateDataSet(ads);
        }

        // Swapping of the datasets should NOT be done while the ObjectTracker is running.
        // 2. So, Stop the tracker first.
        objectTracker.Stop();
    }
}
