﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//This class is directly added as component to the 3D model that should be turned on and off
public class SelectorPart : MonoBehaviour
{
    //The selector part only includes the path to the according picture
    [SerializeField]
    private string picturePath;

    
    public bool isVanishing = true;
    public bool isHighlighted = false;
    [Tooltip("Only necessary if object should not vanish.")]
    public Material normalMat, highlightMat;

    public string getPicturePath()
    {
        return picturePath;
    }
}
