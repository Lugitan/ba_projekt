﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PartSelector : MonoBehaviour
{

    private bool expanded = false;
    private Animator animator;
    private SelectorPart[] mappingItemPart;

    [SerializeField]
    private GameObject content, partedObject, selectorItem;

    // Use this for initialization
    void Start()
    {
        animator = this.GetComponent<Animator>();
        
        Initialize(); 
    }

    public void Initialize()
    {
        //Get all selector items from all childs of the given parent object
        mappingItemPart = partedObject.GetComponentsInChildren<SelectorPart>();

        //Iterate over all items
        for(int i=0; i < mappingItemPart.Length; i++)
        {
            //Instantiate the item based on a prefab
            GameObject item = Instantiate(selectorItem, content.GetComponent<RectTransform>());

            //Set Position
            item.GetComponent<RectTransform>().anchorMin = new Vector2(0.1f, (0.85f - (i * 0.15f)));
            item.GetComponent<RectTransform>().anchorMax = new Vector2(0.9f, (0.95f - (i * 0.15f)));

            //Set Picture
            item.GetComponent<Image>().sprite = Resources.Load<Sprite>(mappingItemPart[i].getPicturePath());
            item.GetComponent<Image>().type = Image.Type.Simple;

            //Set callback function
            int index = i;
            Button itemButton = item.gameObject.GetComponentInChildren<Button>();
            itemButton.onClick.AddListener(() => itemClicked(index));

            //Disable part
            mappingItemPart[i].gameObject.SetActive(true);
        }
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void expandButtonClicked()
    {
        //Expands/close the part selctor menu
        //The menu is closed using states of the animator for the menu
        if (expanded)
        {
            animator.SetTrigger("Close");
            expanded = false;
        }
        else
        {
            animator.SetTrigger("Expand");
            expanded = true;
        }
    }

    public void changedOrientation()
    {
        animator.SetTrigger("Landscape");

        int counter = 0;
        foreach(Transform child in content.transform)
        {
            child.GetComponent<RectTransform>().anchorMin = new Vector2(0.1f, (0.85f - (counter * 0.15f)));
            child.GetComponent<RectTransform>().anchorMax = new Vector2(0.9f, (0.95f - (counter * 0.15f)));
            counter++;
        }
        
    }

    public void itemClicked(int id)
    {
        if (mappingItemPart[id].isVanishing)
        {
            //Activates/Deactivates the according 3D model part if a selector item is clicked
            if (mappingItemPart[id].gameObject.activeSelf)
            {
                mappingItemPart[id].gameObject.SetActive(false);                
            }
            else
            {
                mappingItemPart[id].gameObject.SetActive(true);                
            }
        }
        else
        {
            MeshRenderer[] meshes = mappingItemPart[id].gameObject.GetComponentsInChildren<MeshRenderer>();

            if (mappingItemPart[id].isHighlighted)
            {
                mappingItemPart[id].isHighlighted = false;
                foreach (var mesh in meshes)
                {
                    mesh.material = mappingItemPart[id].normalMat;
                }
            }
            else
            {
                mappingItemPart[id].isHighlighted = true;
                foreach (var mesh in meshes)
                {
                    mesh.material = mappingItemPart[id].highlightMat;
                }
            }   
        }
             
    }
}
