﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

//This script is responsible for changing the color of the selector part if it is clicked
public class FrameScript : MonoBehaviour
{
    private bool selected = false;
    private Image frame;

	// Use this for initialization
	void Start ()
    {
        frame = this.GetComponent<Image>();
        frame.color = new Color(0, 1, 0, 0.4f);
    }
	
	public void ButtonClicked()
    {
        if(selected)
        {
            frame.color = new Color(0,1,0,0.4f);
            selected = false;
        }
        else
        {
            frame.color = new Color(1, 0, 0, 0.4f);
            selected = true;
        }
    }
}
