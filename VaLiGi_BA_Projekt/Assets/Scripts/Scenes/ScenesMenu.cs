﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

//
// Handles the menu in the different exhibit scenes
//
public class ScenesMenu : MonoBehaviour
{

    #region PUBLIC_MEMBERS    

    public string ExhibitKey;
    public GameObject About, Help;
    public Text AboutTitle;
    public Text AboutDescription;
    public UnityEngine.UI.Image AboutImage;
    public Sprite defaultAboutSprite;
    public Sprite firstAboutSprite;

    public bool isAboutVisible = false;

    [Header("Only necessary for the Acropolis")]
    public GameObject edificButton;
    public string edificString;
    public Sprite edificImage;

    [Header("Only necessary for the Parthenon")]
    public GameObject infoText;
    #endregion // PUBLIC_MEMBERS

    // initialize static enum with one of the items
    AcropolisScreenInfo acropolisScreenInfo;

    #region MONOBEHAVIOUR_METHODS

    void Start()
    {
        //About.SetActive(false);
        
        if (ExhibitKey.Equals("Acropolis"))
        {
            if (acropolisScreenInfo == null)
            {
                acropolisScreenInfo = new AcropolisScreenInfo();
            }
        }
    }

    #endregion // MONOBEHAVIOUR_METHODS

    #region PUBLIC_METHODS
    
    public void ToggleInfoText()
    {
        if(infoText != null)
        {
            if (infoText.activeSelf) { infoText.SetActive(false); }
            else
            { infoText.SetActive(true); }
        }        
    }

    public void LoadHelpScreen()
    {
        isAboutVisible = true;

        Help.SetActive(true);
    }

    /// only used in non acropolis scenes
    public void LoadInfoScreen()
    {
        isAboutVisible = true;

        About.SetActive(true);
    }

    public void CloseHelpScreen()
    {
        // called to return to Menu from Help screen      
        isAboutVisible = false;
        Help.SetActive(false);
    }

    public void CloseAboutScreen()
    {
        // called to return to Menu from About screen   
        isAboutVisible = false;
        About.SetActive(false);
        //if(!AboutImage.enabled) AboutImage.enabled = true;

        if (edificButton != null) { edificButton.SetActive(false); }
    }

    public void LoadFirstEdificScreen(string edificKey)
    {
        if (!isAboutVisible)
        {
            AboutImage.sprite = firstAboutSprite;

            AboutTitle.text = acropolisScreenInfo.GetTitle(edificKey);
            AboutDescription.text = acropolisScreenInfo.GetDescription(edificKey);

            LoadAboutScreen();
        }
    }

    /// only used in acropolis sceen
    public void LoadEdificeScreen()
    {
        if (!isAboutVisible)
        {
            if (edificImage != null)
            {
                AboutImage.sprite = edificImage;
            }
            else
            {
                //AboutImage.enabled = false;
            }
            AboutTitle.text = acropolisScreenInfo.GetTitle(edificString);
            AboutDescription.text = acropolisScreenInfo.GetDescription(edificString);

            LoadAboutScreen();
        }
    }

    /// only used in acropolis sceen
    public void ActivateEdificButton(string edificKey, Sprite image)
    {
        edificString = edificKey;
        edificImage = image;
        edificButton.SetActive(true);
    }


    public void SwitchTracking(int sceneId)
    {
        SceneManager.LoadScene(sceneId);
    }

    public void LoadMenuScene()
    {
        MainMenu.LoadScene(MainMenu.MenuScene);
    }

    #endregion // PUBLIC_METHODS

    #region PRIVATE_METHODS

    private void LoadAboutScreen()
    {
            isAboutVisible = true;
            About.SetActive(true);               
    }

    #endregion
    
}
