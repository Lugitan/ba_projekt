﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

//
// handles the info text UI for the InfoCubes
//
public class Info : MonoBehaviour {


    public bool isVisible = false;

    [SerializeField]
    private Canvas canvas;
    [SerializeField]
    private Text title;
    [SerializeField]
    private Text description;

    private void Start()
    {
        canvas.enabled = false;
    }

    public void ShowInfo(string tit, string tex)
    {
        title.text = tit;
        description.text = tex;

        isVisible = true;
        canvas.enabled = true;
    }

    public void CloseInfo()
    {
        isVisible = false;
        canvas.enabled = false;
    }
}
