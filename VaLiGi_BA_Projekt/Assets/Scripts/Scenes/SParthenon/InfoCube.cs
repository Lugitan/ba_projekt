﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using UnityEngine.EventSystems;

//
// holds the information to display and listens for mouse down events
//
public class InfoCube : MonoBehaviour {

    [SerializeField]
    private Info info;

    [SerializeField]
    private string title;

    [SerializeField]
    private string text;
    
    private void OnMouseDown()
    {
        if (!EventSystem.current.IsPointerOverGameObject())
        {
            if (!info.isVisible)
            {
                info.ShowInfo(title, text);
            }
        }
    }
}
