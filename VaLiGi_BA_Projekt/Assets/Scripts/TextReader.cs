﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TextReader : MonoBehaviour {
    public TextAsset textFile;
    public Text NikeInfos;

	// Use this for initialization
	void Start () {
        if (textFile != null)
        {
            string content = textFile.text;
            if (content == "")
                content = System.Text.Encoding.Default.GetString(textFile.bytes);
            NikeInfos.text = content;
            //clues = (content.Split('\n'));

        }
        
    }
	
	// Update is called once per frame
	void Update () {
		
	}
}
