﻿/*===============================================================================
Copyright (c) 2015-2017 PTC Inc. All Rights Reserved.

Copyright (c) 2015 Qualcomm Connected Experiences, Inc. All Rights Reserved.

Vuforia is a trademark of PTC Inc., registered in the United States and other 
countries.
===============================================================================*/
using UnityEngine;
using System.Collections;

public class ModifiedViewTrigger : MonoBehaviour
{
    public enum TriggerType
    {
        VR_TRIGGER,
        AR_TRIGGER
    }

    #region PUBLIC_MEMBER_VARIABLES
    public TriggerType triggerType = TriggerType.VR_TRIGGER;
    public float activationTime = 1.5f;
    public Material focusedMaterial;
    public Material nonFocusedMaterial;
    public bool Focused { get; set; }
    #endregion // PUBLIC_MEMBER_VARIABLES


    #region PRIVATE_MEMBER_VARIABLES
    bool mTriggered;
    ModifiedTransitionManager mTransitionManager;
    Transform cameraTransform;
    #endregion // PRIVATE_MEMBER_VARIABLES


    #region MONOBEHAVIOUR_METHODS
    void Start()
    {
        cameraTransform = Camera.main.transform;
        mTransitionManager = FindObjectOfType<ModifiedTransitionManager>();
        //GetComponent<Renderer>().material = nonFocusedMaterial;
    }

    void Update()
    {
        RaycastHit hit;
        Ray cameraGaze = new Ray(cameraTransform.position, cameraTransform.forward);
        Physics.Raycast(cameraGaze, out hit, Mathf.Infinity);
        Focused = hit.collider && (hit.collider.gameObject == gameObject);


        if (mTriggered)
            return;
        
    }
    #endregion // MONOBEHAVIOUR_METHODS

    #region PUBLIC_METHODS
    public void PlayTransition()
    {

        mTriggered = true;

        // Activate transition from AR to VR or vice versa
        bool goingBackToAR = (triggerType == TriggerType.AR_TRIGGER);
        
        mTransitionManager.Play(goingBackToAR);
        StartCoroutine(ResetAfter(0.3f * mTransitionManager.transitionDuration));


    }

    #endregion


    #region PRIVATE_METHODS
    private IEnumerator ResetAfter(float seconds)
    {
        Debug.Log("Resetting View trigger after: " + seconds);

        yield return new WaitForSeconds(seconds);

        Debug.Log("Resetting View trigger: " + name);

        // Reset variables
        mTriggered = false;
        Focused = false;              
    }
    #endregion // PRIVATE_METHODS
}

