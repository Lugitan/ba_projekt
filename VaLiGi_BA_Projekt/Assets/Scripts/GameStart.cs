﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameStart : MonoBehaviour {
    //Button for Shipgame
    public Button gameButton;
    //Button to change to VR
    public Button EnvironmentButton;
    //making ship visible after playing the game
    public GameObject proraToggle;
    //infoBox only available after playing the game
    public GameObject infoBox;
    //for testing
    public bool cheatWonGame = false;

	// Use this for initialization
	void Start () {
        //not won game yet
        proraToggle.SetActive(false);
        infoBox.SetActive(false);
        //when clicked on Button
        gameButton.onClick.AddListener(StartGame);
        EnvironmentButton.onClick.AddListener(TaskOnClick2);

        //cheat for testing
        if (cheatWonGame)
        {
            GameMechanic.setWonGame(true);
        }
    }

    private void OnValidate()
    {
        if(cheatWonGame)
            GameMechanic.setWonGame(true);
    }

    //start game
    void StartGame()
    {
        GameMechanic.setWonGame(false);
        SceneManager.LoadScene("GameMenu");
    }
    //change scene to VR
    void TaskOnClick2()
    {
        SceneManager.LoadScene("WellSystemClose");
    }

    void Update()
    {
        //if game was won and toggle has not been activated, activate the toggle
        if((GameMechanic.getWonGame()) && !proraToggle.activeSelf)
        {
            proraToggle.SetActive(true);
        }
    }
}
