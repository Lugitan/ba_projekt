﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Vuforia;
using UnityEngine.UI;

public class NikeFakeAR : MonoBehaviour
{
    //invisible GameButton to fake capturing
    public Button marker;
    public GameObject foundButton;
    //all gameobjects, which should be shown when faking capturing
    public GameObject nike;
    public GameObject nikeArmLeft;
    public GameObject nikeArmRight;
    public GameObject laurelsWreath;
    public GameObject stylis;
    public GameObject taenia;
    public GameObject reconstruction;
    public GameObject prora;
    //toggle to activate/deactivate gameObjects 
    public Toggle leftArm;
    public Toggle rightArm;
    public Toggle laurelT;
    public Toggle stylisT;
    public Toggle taeniaT;
    public Toggle reconT;
    public Toggle proraT;
    //variables to show/hide outline and activate/deactivate ModelTarget
    public ModelTargetBehaviour nikeMT;
    public GameObject nikeTarget;

    private bool markerPressed;
    private DefaultTrackableEventHandler dtrack;
    private bool nikeTracked;
    // Use this for initialization
    void Start()
    {

        //deactivate and hide gameObjects until the reactivated
        nike.SetActive(false);
        nikeArmLeft.SetActive(false);
        nikeArmRight.SetActive(false);
        laurelsWreath.SetActive(false);
        stylis.SetActive(false);
        taenia.SetActive(false);
        reconstruction.SetActive(false);
        prora.SetActive(false);
        //invisible marker button calling method to fake capturing
        marker.onClick.AddListener(foundMarker);
        // marker not pressed in beginning
        markerPressed = false;
        //nike not tracked in the beginning
        nikeTracked = false;
    }

    //method to fake that Statue has been captured
    void foundMarker()
    {
        markerPressed = true;
        nike.SetActive(true);
        nikeMT.GuideViewMode = ModelTargetBehaviour.GuideViewDisplayMode.NoGuideView;
        nikeTarget.SetActive(false);
    }
     //visualize reconstruction
    void ShowAdditions(GameObject go, Toggle t)
    {
        if ((t.isOn == true) && (markerPressed == true))
        {
            go.SetActive(true);
        }
        else if ((t.isOn == false) && (markerPressed == true))
        {
            go.SetActive(false);
        }
    }
    void deactivate()
    {
        nike.SetActive(false);
        nikeArmLeft.SetActive(false);
        nikeArmRight.SetActive(false);
        laurelsWreath.SetActive(false);
        stylis.SetActive(false);
        taenia.SetActive(false);
        reconstruction.SetActive(false);
        prora.SetActive(false);
        markerPressed = false;
        foundButton.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {   
        //if button not pressed, check if Nike has been captured
        if (markerPressed == false)
        {
            nikeTracked = dtrack.found;
        }
        //if Nike tracked deactivate fake AR
        else if (nikeTracked == true)
        {
            deactivate();
        }
        //visualize additional reconstructions if button is pressed
        ShowAdditions(nikeArmLeft, leftArm);
        ShowAdditions(nikeArmRight, rightArm);
        ShowAdditions(laurelsWreath, laurelT);
        ShowAdditions(taenia, taeniaT);
        ShowAdditions(stylis, stylisT);
        ShowAdditions(reconstruction, reconT);
        ShowAdditions(prora, proraT);
    }
}