=======================================================GENERAL=================================================================
UIController deals with initial settings that are used by navigational elements. It controls the distribution of necessary functionalities within the scene.

Main Classes, that are referenced by buttons and navigational elements (onClick events):
- MainNavigationHandler
- SubNavigationHandler
- HelpNavigationHandler
- InfoButtonHandler
- ChangeScene

All panels are positioned outside of the screen space, in order to make it easier for customization in the Unity scene. 
They will all be positioned with the "PanelPositionHandler", that moves every element with the tag "UIPanel" to the screen space when starting the application.
Individual Panels outside of the main groups that this handler holds, can be added to an individual Panel Group.

The navigation works by enableing and disabling panels. Navigation work by layering panels in order of necessity. If the subnavigation is active then the Panels are activated in the order they are navigated from. 

======================UIController=====================================
- Upon activating the buttons in the element, values are being updated in the Scene with OnValidate. Buttons like ARVRButton or InfoButton can be activated or deacivated with an indication in color as well as setting the interactable option of the buttons.
- When the application is started, all other settings that are set in with this element are being assinged. These settings include the assignment of the loading and closing icons of buttons as well as the assignments of ChangeScene

======================MainNavigationHandler============================
- This handler is resposible for the Main Navigation Menu at the bottom of the application
- They are set by dragging the MainPanels element onto the empty Canvas property
- it will fill the list with assigned panels which in turn hold the panels themselves, as well as the title and the activating button as properties
- With the "Is Main Screen" option the top left and right groups of interaction elements can be enabled or disabled respectively. These groups are assigned below the list
- The "Active Menu Background" is a grey Panel to indicate which Menu Element is currently selected
- The title element will recieve the title string from the list elements

- All Main Menu Buttons should have the MainNavigationHandler set as their onClick event with the function ChangePanel() function assigned

===================SubNavigationHandler================================
- This handler is responsible for navigation within a MainPanel group
- If a navigation is started within a panel group, the current panel will be added to a list of previously visited panels. Additionally the "Back Button" in the top left corner of the UI gets activated, in order to navigate back to the previous panel
- If another panel in the MainNavigation is selected, the sub navigation gets cleared by emptying the list

- Buttons within a panel have to have the SubNavigationHandler set as their onClick event with the ChangePanel() function assigned
- Every Button that uses this functionaltiy also should have the SubButtonInfo class assigned with the properties of their parent panel and the panel to navigate to assigned

==================HelpNavigationHandler===============================
- This handler is responsible for the activation and deactivation of elements during the tutorial
- Because of the possibility of a changing screen aspect ratio, the actual interaction elements have to be acitvated
- Also this Handler will also change the Main Menu Help Button to display a different button. It will be used to indicate that the user can cancel the Tutorial and go back to the overvie

- All Tutorial Buttons should have the HelpNavigationHandler set as their onClick event with the StartHelp() function assigned

=================InfoButtonHandler====================================
- This handler is responsible for the display of the Information Panel that is set in the UIController
- This way it is possible to assign panels dynamically if more than one exhibit is availible in one scene. This functionallity to assign the information according to a tracked object still has to be implemented

- The Information Button in the top right corner of the UI should have the InfoButtonHandler set as its onClick event with the ViewInfo() function assigned

=================ChangeScene==========================================
- If it is necessary to switch between scenes, then this class can be called or assigned to a button
- It will first display a loading Panel to indicate to the user that the application is in the state of loading the new scene


=================================================UI ELEMENTS==================================================================
The elements in the scene are ordered by functionality and category of usage. 

- MainPanels: All panels that will be called by the MainNavigationHandler
- SubPanels: All panels that are part of a navigation within a MainMenu Panel
- InfoPanels: Panels that are being used as assingment for the InfoButton. Either Custom Panels can be used or a already create InfoBoxPanel, wich can display dynamically set Text with multiple Pages within the Box

Disabled Objects that are activated dynamically
- CancelHelpButton: Displayed while a tutrial has been started. Indicates to cancel the Tutorail and go back to the HelpOverview
- BackButton: Used for the SubNavigation to go to the previous panel. onClick is being set within the SubNavigationHandler
- CloseInfoButton: Used to close the information panel. onClick is being set within the InfoButtonHandler
- 