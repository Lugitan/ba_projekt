This file contains all elements and components that are currently being developed, that need to be implemented or suggestions for future extensions.

=====================================In Development=============================================

- Import aleady created Menu Panels from Ba_K�nig as unitypackage
	> MainMenuPanel is imported, but Subscreens still missing


====================================Needs to be Implemented=====================================

- Restructure the info Panel functionality to be set as Prefab
	> Create fix Info Panel and use Information that is being displayed as dynamic imports (InfoBox functionality already in place)
	> Restructure the component of the Object placement in UIController to have more Prefab presets present (ease of apply and revert)